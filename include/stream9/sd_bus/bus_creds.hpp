#ifndef STREAM9_SD_BUS_BUS_CREDS_HPP
#define STREAM9_SD_BUS_BUS_CREDS_HPP

#include "cstring_view.hpp"

#include <span>

#include <systemd/sd-bus.h>

namespace stream9::sd_bus {

class bus;

class bus_creds
{
public:
    bus_creds(pid_t, uint64_t creds_mask);
    bus_creds(::sd_bus_creds&) noexcept;

    ~bus_creds() noexcept;

    bus_creds(bus_creds const& other) noexcept;
    bus_creds& operator=(bus_creds const& other) noexcept;

    bus_creds(bus_creds&&) = delete;
    bus_creds& operator=(bus_creds&&) = delete;

    // query
    uint64_t mask() const noexcept;
    uint64_t augmented_mask() const noexcept;

    std::optional<pid_t> pid() const;
    std::optional<pid_t> ppid() const;
    std::optional<pid_t> tid() const;

    std::optional<uid_t> uid() const;
    std::optional<uid_t> euid() const;
    std::optional<uid_t> suid() const;
    std::optional<uid_t> fsuid() const;
    std::optional<uid_t> owner_uid() const;

    std::optional<gid_t> gid() const;
    std::optional<gid_t> egid() const;
    std::optional<gid_t> sgid() const;
    std::optional<gid_t> fsgid() const;
    std::span<gid_t const> supplementary_gids() const;

    std::optional<cstring_view> comm() const;
    std::optional<cstring_view> tid_comm() const;
    std::optional<cstring_view> exe() const;

    cstring_view* cmdline() const; //TODO span

    std::optional<cstring_view> cgroup() const;
    std::optional<cstring_view> unit() const;
    std::optional<cstring_view> slice() const;
    std::optional<cstring_view> user_unit() const;
    std::optional<cstring_view> user_slice() const;
    std::optional<cstring_view> session() const;
    std::optional<cstring_view> selinux_context() const;
    std::optional<cstring_view> tty() const;
    std::optional<cstring_view> description() const;

    std::optional<cstring_view> unique_name() const;
    cstring_view* well_known_names() const; //TODO span

    std::optional<bool> has_effective_cap(int capability) const;
    std::optional<bool> has_permitted_cap(int capability) const;
    std::optional<bool> has_inheritable_cap(int capability) const;
    std::optional<bool> has_bounding_cap(int capability) const;

    std::optional<uint32_t> audit_session_id() const;
    std::optional<uid_t> audit_login_uid() const;

private:
    ::sd_bus_creds* m_handle; // non-null
};

bus_creds name_creds(bus&, cstring_view name, uint64_t mask);

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_BUS_CREDS_HPP
