#ifndef STREAM9_SD_BUS_SLOT_HPP
#define STREAM9_SD_BUS_SLOT_HPP

#include "cstring_view.hpp"
#include "message_handler.hpp"

#include <optional>
#include <memory>

#include <systemd/sd-bus.h>

namespace stream9::sd_bus {

class bus;
class message;
struct slot_data;

class [[nodiscard]] slot
{
public:
    // essential
    slot(::sd_bus_slot&); // take ownership. it won't increment ref count
    slot(::sd_bus_slot&, std::unique_ptr<slot_data>);

    ~slot() noexcept;

    slot(slot const&) noexcept;
    slot& operator=(slot const&) noexcept;

    slot(slot&&) noexcept;
    slot& operator=(slot&&) noexcept;

    void swap(slot&) noexcept;

    // query
    class bus                   bus() const noexcept;
    void*                       userdata() const noexcept;
    std::optional<cstring_view> description() const;
    bool                        is_floating() const;
    ::sd_bus_destroy_t          destroy_callback() const noexcept;

    std::optional<message> current_message() const;
    message_handler const& current_handler() const noexcept;
    void*                  current_userdata() const noexcept;

    // modifier
    void* set_userdata(void*) noexcept;
    void set_user_description(cstring_view);
    void set_floating(bool enable);
    void set_destroy_callback(::sd_bus_destroy_t callback) noexcept;

    // conversion
    operator ::sd_bus_slot* () const { return m_handle; }

private:
    ::sd_bus_slot* m_handle; // non-null
};

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_SLOT_HPP
