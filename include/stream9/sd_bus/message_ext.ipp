#ifndef STREAM9_SD_BUS_MESSAGE_EXT_IPP
#define STREAM9_SD_BUS_MESSAGE_EXT_IPP

#include "message_ext.hpp"

#include "error.hpp"

#include <system_error>

#include <string.h>

namespace stream9::sd_bus {

template<std::invocable<> Fn>
void
read_container(message& m, char type, signature contents, Fn&& read_contents)
{
    m.enter_container(type, contents);

    read_contents();

    m.exit_container();
}

template<std::invocable<> Fn>
void
read_array(message& m, array_contents_signature c, Fn&& read_contents)
{
    try {
        read_container(m, SD_BUS_TYPE_ARRAY, c,
                       std::forward<Fn>(read_contents) );
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

template<std::invocable<> Fn>
void
read_struct(message& m, struct_contents_signature c, Fn&& read_contents)
{
    try {
        read_container(m, SD_BUS_TYPE_STRUCT, c,
                       std::forward<Fn>(read_contents) );
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }

}

template<std::invocable<> Fn>
void
read_variant(message& m, variant_contents_signature c, Fn&& read_contents)
{
    try {
        read_container(m, SD_BUS_TYPE_VARIANT, c,
                       std::forward<Fn>(read_contents) );
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

template<std::invocable<> Fn>
void
read_dict_entry(message& m, dict_entry_contents_signature c, Fn&& read_contents)
{
    try {
        read_container(m, SD_BUS_TYPE_DICT_ENTRY, c,
                       std::forward<Fn>(read_contents) );
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

template<typename V, char C>
void
append_trivial_array(message& m, contiguous_range_of<V> auto&& a)
{
    void* p {};
    auto mem_size = alignof(V) * rng::size(a);

    m.append_array_space(C, mem_size, &p);

    ::memcpy(p, rng::data(a), mem_size);
}

inline void
append_byte_array(message& m, contiguous_range_of<uint8_t> auto&& a)
{
    try {
        using R = decltype(a);
        append_trivial_array<uint8_t, SD_BUS_TYPE_BYTE>(m, std::forward<R>(a));
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

inline void
append_int16_array(message& m, contiguous_range_of<int16_t> auto&& a)
{
    try {
        using R = decltype(a);
        append_trivial_array<int16_t, SD_BUS_TYPE_INT16>(m, std::forward<R>(a));
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

inline void
append_uint16_array(message& m, contiguous_range_of<uint16_t> auto&& a)
{
    try {
        using R = decltype(a);
        append_trivial_array<uint16_t, SD_BUS_TYPE_UINT16>(m, std::forward<R>(a));
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

inline void
append_int32_array(message& m, contiguous_range_of<int32_t> auto&& a)
{
    try {
        using R = decltype(a);
        append_trivial_array<int32_t, SD_BUS_TYPE_INT32>(m, std::forward<R>(a));
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

inline void
append_uint32_array(message& m, contiguous_range_of<uint32_t> auto&& a)
{
    try {
        using R = decltype(a);
        append_trivial_array<uint32_t, SD_BUS_TYPE_UINT32>(m, std::forward<R>(a));
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

inline void
append_int64_array(message& m, contiguous_range_of<int64_t> auto&& a)
{
    try {
        using R = decltype(a);
        append_trivial_array<int64_t, SD_BUS_TYPE_INT64>(m, std::forward<R>(a));
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

inline void
append_uint64_array(message& m, contiguous_range_of<uint64_t> auto&& a)
{
    try {
        using R = decltype(a);
        append_trivial_array<uint64_t, SD_BUS_TYPE_UINT64>(m, std::forward<R>(a));
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

inline void
append_double_array(message& m, contiguous_range_of<double> auto&& a)
{
    try {
        using R = decltype(a);
        append_trivial_array<double, SD_BUS_TYPE_DOUBLE>(m, std::forward<R>(a));
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

template<std::invocable<> Fn>
void
append_container(message& m, char type,
                 cstring_view contents, Fn&& append_contents)
{
    m.open_container(type, contents);

    append_contents();

    m.close_container();
}

template<std::invocable<> Fn>
void
append_array(message& m, array_contents_signature c, Fn&& append_contents)
{
    try {
        append_container(m, SD_BUS_TYPE_ARRAY, c,
                         std::forward<Fn>(append_contents) );
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

template<std::invocable<> Fn>
void
append_struct(message& m, struct_contents_signature c, Fn&& append_contents)
{
    try {
        append_container(m, SD_BUS_TYPE_STRUCT, c,
                         std::forward<Fn>(append_contents) );
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

template<std::invocable<> Fn>
void
append_variant(message& m, variant_contents_signature c, Fn&& append_contents)
{
    try {
        append_container(m, SD_BUS_TYPE_VARIANT, c,
                         std::forward<Fn>(append_contents) );
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

template<std::invocable<> Fn>
void
append_dict_entry(message& m, dict_entry_contents_signature c, Fn&& append_entry)
{
    try {
        append_container(m, SD_BUS_TYPE_DICT_ENTRY, c,
                         std::forward<Fn>(append_entry) );
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}


} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_MESSAGE_EXT_IPP
