#ifndef STREAM9_SD_BUS_AWAITER_HPP
#define STREAM9_SD_BUS_AWAITER_HPP

#include "bus.hpp"
#include "duration.hpp"
#include "error.hpp"
#include "message.hpp"
#include "slot.hpp"

#include <coroutine>
#include <cstdarg>
#include <optional>
#include <source_location>

#include <systemd/sd-bus.h>

namespace stream9::sd_bus {

class message_awaiter
{
public:
    // essential
    message_awaiter() = default;
    ~message_awaiter() noexcept;

    message_awaiter(message_awaiter const&) noexcept;
    message_awaiter& operator=(message_awaiter const&) noexcept;

    message_awaiter(message_awaiter&&) noexcept;
    message_awaiter& operator=(message_awaiter&&) noexcept;

    void swap(message_awaiter&) noexcept;

    // awaiter
    constexpr bool await_ready() const noexcept { return false; }

    method_return await_resume() const;

protected:
    void set_handle(std::coroutine_handle<>);
    void set_slot(::sd_bus_slot*);
    void set_location(std::source_location);

protected:
    static int callback(::sd_bus_message*, void* userdata, ::sd_bus_error*);

private:
    std::coroutine_handle<> m_handle;
    std::optional<::sd_bus_slot*> m_slot;
    std::optional<method_return> m_result;
    std::optional<dbus_error> m_error;
    std::source_location m_loc;
};

class call_awaiter : public message_awaiter
{
public:
    call_awaiter(bus&, method_call&, duration timeout);

    void await_suspend(std::coroutine_handle<>,
                       std::source_location = std::source_location::current() );

private:
    bus m_bus;
    method_call m_message;
    uint64_t m_usec;
};

class call_method_awaiter : public message_awaiter
{
public:
    call_method_awaiter(bus&,
                        cstring_view destination,
                        cstring_view path,
                        cstring_view interface,
                        cstring_view member,
                        cstring_view type, va_list va);

    void await_suspend(std::coroutine_handle<>,
                       std::source_location = std::source_location::current() );
};

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_AWAITER_HPP
