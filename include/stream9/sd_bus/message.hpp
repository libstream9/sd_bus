#ifndef STREAM9_SD_BUS_MESSAGE_HPP
#define STREAM9_SD_BUS_MESSAGE_HPP

#include "cstring_view.hpp"
#include "duration.hpp"
#include "error.hpp"

#include <memory>
#include <optional>
#include <utility>

#include <systemd/sd-bus.h>

namespace stream9::sd_bus {

class bus;
class bus_creds;

class [[nodiscard]] message
{
public:
    // essential
    message(class bus&, uint8_t type);

    message(::sd_bus_message&) noexcept;

    ~message() noexcept;

    message(message const&) noexcept;
    message& operator=(message const&) noexcept;

    message(message&&) noexcept;
    message& operator=(message&&) noexcept;

    void swap(message&) noexcept;

    // query
    uint8_t type() const;

    bool is_signal(cstring_view interface = nullptr,
                   cstring_view member = nullptr) const;
    bool is_method_call(cstring_view interface = nullptr,
                        cstring_view member = nullptr) const;
    bool is_method_error(cstring_view name = nullptr) const;
    bool is_empty() const noexcept;

    bool expect_reply() const noexcept;
    bool auto_start() const noexcept;
    bool allow_interactive_authorization() const noexcept;
    bool has_signature(cstring_view signature) const noexcept;

    cstring_view signature(bool complete = false) const noexcept;
    cstring_view path() const noexcept;
    cstring_view interface() const noexcept;
    cstring_view member() const noexcept;
    cstring_view destination() const noexcept;
    cstring_view sender() const noexcept;

    class bus bus() const noexcept;
    bus_creds creds() const noexcept;
    std::optional<uint64_t> cookie() const;
    std::optional<uint64_t> reply_cookie() const;
    std::optional<duration> monotonic_usec() const;
    std::optional<duration> realtime_usec() const;
    std::optional<uint64_t> seqnum() const;

    void read(cstring_view type, ...);
    void read_basic(char type, void* p);
    void read_array(char type, void const**, size_t* size);
    std::unique_ptr<char**> read_strv();

    void enter_container(char type, cstring_view contents);
    void exit_container();

    bool peek_type(char* type, char const** contents);

    struct peek_type_result {
        char type;
        cstring_view contents;
    };

    std::optional<peek_type_result> peek_type();

    bool verify_type(char type, cstring_view contents);

    bool at_end(bool complete = false);

    std::optional<class dbus_error> dbus_error() const noexcept; //TODO move to method_error
    int get_errno() const noexcept;

    // modifier
    void set_expect_reply(bool enable);
    void set_auto_start(bool enable);
    void set_allow_interactive_authorization(bool enable);
    void set_destination(cstring_view destination);
    void set_sender(cstring_view sender);
    void mark_sensitive();

    void append(cstring_view type, ...);
    void append_basic(char type, void const* p);
    void append_array(char type, void const* ptr, size_t size);
    void append_array_space(char type, size_t size, void** ptr);
    void append_array_iovec(char type, struct ::iovec const* iov, unsigned n);
    void append_array_memfd(char type, int memfd, uint64_t offset, uint64_t size);
    void append_string_space(size_t size, char**s);
    void append_string_iovec(struct ::iovec const*, unsigned n);
    void append_string_memfd(int memfd, uint64_t offset, uint64_t size);
    void append_strv(char** l);

    void open_container(char type, cstring_view contents);
    void close_container();

    void copy(message const& source, bool all);

    // command
    void seal(uint64_t cookie, duration timeout = {});

    void skip(cstring_view type);
    void rewind(bool complete = false);

    void dump(FILE* f, uint64_t flags) const;

    // conversion
    operator ::sd_bus_message* () const { return m_handle; }

private:
    ::sd_bus_message* m_handle; // non-null
};

class signal : public message
{
public:
    signal(class bus&,
           cstring_view path,
           cstring_view interface,
           cstring_view member);

    signal(::sd_bus_message&) noexcept;
};

class method_call : public message
{
public:
    method_call(class bus&,
                cstring_view destination,
                cstring_view path,
                cstring_view interface,
                cstring_view member);

    method_call(::sd_bus_message&) noexcept;
};

class method_return : public message
{
public:
    method_return(message const&);

    method_return(::sd_bus_message&) noexcept;
};

class method_error : public message
{
public:
    method_error(method_call const&, ::sd_bus_error const&);
    method_error(method_call const&, int error, ::sd_bus_error const* = nullptr);

    method_error(::sd_bus_message&) noexcept;
};

method_error
    make_method_error(method_call const&, //TODO do this with macro
                      cstring_view name, cstring_view format, ...);

method_error
    make_method_error(method_call const&, //TODO
                      int error, cstring_view format, ...);

void reply_method_return(method_call const&, cstring_view types, ...);

void reply_method_error(method_call const&, dbus_error const&);
void reply_method_error(method_call const&,
                        cstring_view name, cstring_view format, ...);

void reply_method_errno(method_call const&, int error, dbus_error const&);
void reply_method_errno(method_call const&, int error, cstring_view format, ...);

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_MESSAGE_HPP
