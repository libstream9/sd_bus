#ifndef STREAM9_SD_BUS_VTABLE_HPP
#define STREAM9_SD_BUS_VTABLE_HPP

#include "cstring_view.hpp"
#include "message_handler.hpp"
#include "property_handler.hpp"

#include <memory>
#include <vector>

#include <systemd/sd-bus.h>

namespace stream9::sd_bus {

union vtable_data;

class vtable
{
public:
    // essential
    vtable(uint64_t flags = 0);

    ~vtable() noexcept;

    vtable(vtable&&) noexcept;
    vtable& operator=(vtable&&) noexcept;

    // accessor
    ::sd_bus_vtable const* table() const noexcept { return m_vtbl.data(); }
    auto data() const noexcept { return m_data.data()->get(); }

    // modifier
    void add_method(cstring_view member,
                    cstring_view args,
                    cstring_view result,
                    message_handler,
                    uint64_t flags = 0);

    void add_signal(cstring_view member,
                    cstring_view signature,
                    uint64_t flags = 0);

    void add_property(cstring_view member,
                      cstring_view signature,
                      property_handler get,
                      uint64_t flags = 0);

    void add_property(cstring_view member,
                      cstring_view signature,
                      property_handler get,
                      property_handler set,
                      uint64_t flags = 0);

private:
    std::vector<::sd_bus_vtable> m_vtbl;

    std::vector<std::unique_ptr<vtable_data>> m_data;
};

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_VTABLE_HPP
