#ifndef STREAM9_SD_BUS_DURATION_HPP
#define STREAM9_SD_BUS_DURATION_HPP

#include <chrono>

namespace stream9::sd_bus {

using duration = std::chrono::duration<uint64_t, std::micro>;

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_DURATION_HPP
