#ifndef STREAM9_SD_BUS_CSTRING_ARRAY_HPP
#define STREAM9_SD_BUS_CSTRING_ARRAY_HPP

#include <stream9/c.hpp>

namespace stream9::sd_bus {

using cstring_array = c::array<c::const_string>;

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_CSTRING_ARRAY_HPP
