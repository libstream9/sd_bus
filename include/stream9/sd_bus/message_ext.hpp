#ifndef STREAM9_SD_BUS_MESSAGE_EXT_HPP
#define STREAM9_SD_BUS_MESSAGE_EXT_HPP

#include "cstring_view.hpp"
#include "message.hpp"
#include "signature.hpp"

#include <concepts>
#include <cstdint>
#include <span>

namespace stream9::sd_bus {

/*
 * read basic type
 */
uint8_t      read_byte(message&);
bool         read_boolean(message&);
int16_t      read_int16(message&);
uint16_t     read_uint16(message&);
int32_t      read_int32(message&);
uint32_t     read_uint32(message&);
int64_t      read_int64(message&);
uint64_t     read_uint64(message&);
double       read_double(message&);
cstring_view read_string(message&);
cstring_view read_object_path(message&);
cstring_view read_signature(message&);
int          read_unix_fd(message&);

/*
 * read array of trivial type
 */
std::span<uint8_t const>  read_byte_array(message&);
std::span<int const>      read_boolean_array(message&);
std::span<int16_t const>  read_int16_array(message&);
std::span<uint16_t const> read_uint16_array(message&);
std::span<int32_t const>  read_int32_array(message&);
std::span<uint32_t const> read_uint32_array(message&);
std::span<int64_t const>  read_int64_array(message&);
std::span<uint64_t const> read_uint64_array(message&);
std::span<double const>   read_double_array(message&);

/*
 * read container
 */
template<std::invocable<> Fn>
void read_array(message&, array_contents_signature, Fn&& read_contents);

template<std::invocable<> Fn>
void read_struct(message&, struct_contents_signature, Fn&& read_contents);

template<std::invocable<> Fn>
void read_variant(message&, variant_contents_signature, Fn&& read_contents);

template<std::invocable<> Fn>
void read_dict_entry(message&, dict_entry_contents_signature, Fn&& read_contents);

/*
 * append basic type
 */
void append_byte(message&, uint8_t);
void append_boolean(message&, bool);
void append_int16(message&, int16_t);
void append_uint16(message&, uint16_t);
void append_int32(message&, int32_t);
void append_uint32(message&, uint32_t);
void append_int64(message&, int64_t);
void append_uint64(message&, uint64_t);
void append_double(message&, double);
void append_string(message&, cstring_view);
void append_object_path(message&, cstring_view);
void append_signature(message&, cstring_view);
void append_unix_fd(message&, int);

/*
 * append array of trivial type
 */
template<typename R, typename T>
concept contiguous_range_of = rng::contiguous_range<R>
                           && std::same_as<rng::range_value_t<R>, T>;

// no boolean variant because sd_bus_message_append_array_space() doesn't seem to support boolean
void append_byte_array(message&, contiguous_range_of<uint8_t> auto&&);
void append_int16_array(message&, contiguous_range_of<int16_t> auto&&);
void append_uint16_array(message&, contiguous_range_of<uint16_t> auto&&);
void append_int32_array(message&, contiguous_range_of<int32_t> auto&&);
void append_uint32_array(message&, contiguous_range_of<uint32_t> auto&&);
void append_int64_array(message&, contiguous_range_of<int64_t> auto&&);
void append_uint64_array(message&, contiguous_range_of<uint64_t> auto&&);
void append_double_array(message&, contiguous_range_of<double> auto&&);

/*
 * append container
 */
template<std::invocable<> Fn>
void append_array(message&, array_contents_signature, Fn&& append_contents);

template<std::invocable<> Fn>
void append_struct(message&, struct_contents_signature, Fn&& append_contents);

template<std::invocable<> Fn>
void append_variant(message&, variant_contents_signature, Fn&& append_contents);

template<std::invocable<> Fn>
void append_dict_entry(message&, dict_entry_contents_signature, Fn&& append_entry);

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_MESSAGE_EXT_HPP

#include "message_ext.ipp"
