#ifndef STREAM9_SD_BUS_CSTRING_VIEW_HPP
#define STREAM9_SD_BUS_CSTRING_VIEW_HPP

#include <stream9/strings.hpp>

namespace stream9::sd_bus {

using stream9::strings::cstring_view;

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_CSTRING_VIEW_HPP
