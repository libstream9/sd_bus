#ifndef STREAM9_SD_BUS_JSON_HPP
#define STREAM9_SD_BUS_JSON_HPP

#include <stream9/json.hpp>

namespace stream9::sd_bus {

class message;
class slot;

void tag_invoke(json::value_from_tag, json::value&, message&);
void tag_invoke(json::value_from_tag, json::value&, slot&);

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_JSON_HPP
