#ifndef STREAM9_SD_BUS_BUS_HPP
#define STREAM9_SD_BUS_BUS_HPP

#include "cstring_view.hpp"
#include "duration.hpp"
#include "message_handler.hpp"
#include "namespace.hpp"

#include <optional>

#include <systemd/sd-bus.h>

#include <stream9/strings.hpp>

namespace stream9::sd_event { class event; }

namespace stream9::sd_bus {

class bus_creds;
class message;
class method_call;
class object;
class slot;
class vtable;

class [[nodiscard]] bus
{
public:
    // essential
    bus(cstring_view address);
    bus(cstring_view exec_path, char* const* argv);
    bus(int input_fd, int output_fd);

    bus(::sd_bus&) noexcept; // take ownership, doesn't increment ref count

    ~bus() noexcept;

    bus(bus const& other) noexcept;
    bus& operator=(bus const& other) noexcept;

    bus(bus&& other) noexcept;
    bus& operator=(bus&& other) noexcept;

    void swap(bus&) noexcept;

    // query
    bool is_open() const;
    bool is_ready() const;
    bool is_anonymous() const;
    bool is_trusted() const;
    bool is_client() const;
    bool is_server() const;
    bool is_monitor() const;
    bool allow_interactive_authorization() const;
    bool signal_connected() const;
    bool exit_on_disconnect() const;
    bool watch_bind() const;
    bool can_send(char type) const;
    int fd() const;
    int events() const;
    pid_t tid() const;
    ::sd_id128_t bus_id() const;
    duration timeout() const;
    duration method_call_timeout() const;
    uint64_t creds_mask() const;
    uint64_t n_queued_read() const;
    uint64_t n_queued_write() const;
    cstring_view description() const;
    std::optional<cstring_view> scope() const;
    cstring_view unique_name() const;
    std::optional<cstring_view> address() const;
    std::optional<cstring_view> default_sender() const;
    std::optional<class sd_event::event> event() const;

    ::sd_bus_message_handler_t current_handler() const noexcept;
    std::optional<message>     current_message() const noexcept;
    std::optional<slot>        current_slot() const noexcept;
    void*                      current_userdata() const noexcept;

    bus_creds creds(uint64_t mask) const;

    // modifier
    void set_anonymous(bool enable);
    void set_trusted(bool enable);
    void set_client(bool enable);
    void set_server(bool enable, ::sd_id128_t);
    void set_monitor(bool enable);
    void set_allow_interactive_authorization(bool enable);
    void set_signal_connected(bool enable);
    void set_exit_on_disconnect(bool enable);
    void set_watch_bind(bool enable);
    void set_negotiate_fds(bool enable);
    void set_negotiate_timestamp(bool enable);
    void set_negotiate_creds(bool enable, uint64_t mask);
    void set_method_call_timeout(duration timeout);
    void set_description(cstring_view);
    void set_default_sender(cstring_view);

    slot add_match(cstring_view match, message_handler);

    slot add_match_async(cstring_view match,
                         message_handler callback,
                         ::sd_bus_message_handler_t install_callback );

    slot match_signal(cstring_view sender,
                      cstring_view path,
                      cstring_view interface,
                      cstring_view member,
                      message_handler);

    slot match_signal_async(cstring_view sender,
                            cstring_view path,
                            cstring_view interface,
                            cstring_view member,
                            message_handler callback,
                            ::sd_bus_message_handler_t add_callback);

    slot add_filter(message_handler);

    slot add_object(cstring_view path, message_handler);

    object add_object(cstring_view path, cstring_view interface,
                      vtable&&);

    slot add_fallback(cstring_view prefix, message_handler);

    ::sd_bus_slot* add_fallback(cstring_view prefix, cstring_view interface,
                                ::sd_bus_vtable const vtbl[],
                                ::sd_bus_object_find_t,
                                void* userdata );

    ::sd_bus_slot* add_node_enumerator(cstring_view path,
                                       ::sd_bus_node_enumerator_t,
                                       void* userdata);

    ::sd_bus_slot* add_object_manager(cstring_view path);

    void attach_event(class sd_event::event&, int priority = SD_EVENT_PRIORITY_NORMAL);
    void attach_event(int priority = SD_EVENT_PRIORITY_NORMAL);
    void detach_event();

    void enqueue_for_read(message&);

    // command
    void start();
    void close() noexcept;
    void flush();

    void wait(duration timeout);
    std::optional<message> process();

    // conversion
    operator ::sd_bus* () const { return m_handle; }

private:
    ::sd_bus* m_handle; // non-null
};

bus default_bus();
bus default_system_bus();
bus default_user_bus();
bus open_bus(cstring_view description = nullptr);
bus open_user_bus(cstring_view description = nullptr);
bus open_system_bus(cstring_view description = nullptr);
bus open_user_machine(cstring_view machine);
bus open_system_machine(cstring_view machine);
bus open_system_remote(cstring_view host);

inline void
default_flush_close()
{
    ::sd_bus_default_flush_close();
}

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_BUS_HPP
