#ifndef STREAM9_SD_BUS_CSTRING_HPP
#define STREAM9_SD_BUS_CSTRING_HPP

#include <stream9/c.hpp>

namespace stream9::sd_bus {

using const_cstring = stream9::c::const_string;

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_CSTRING_HPP
