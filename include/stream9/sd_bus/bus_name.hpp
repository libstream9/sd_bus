#ifndef STREAM9_SD_BUS_BUS_NAME_HPP
#define STREAM9_SD_BUS_BUS_NAME_HPP

#include "error.hpp"

#include <systemd/sd-bus.h>

#include <stream9/strings.hpp>

namespace stream9::sd_bus {

class bus_name
{
public:
    bus_name(bus& b, cstring_view name, uint64_t flags)
        : m_bus { &b }
        , m_name { name }
    {
        auto const rv = ::sd_bus_request_name(*m_bus, name, flags);
        if (rv < 0) {
            throw error { "sd_bus_request_name", rv };
        }
    }

    ~bus_name() noexcept
    {
        ::sd_bus_release_name(*m_bus, m_name);
    }

    operator char const* () const
    {
        return m_name;
    }

    operator cstring_view () const
    {
        return m_name;
    }

private:
    bus* m_bus; // non-null
    cstring_view m_name;
};

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_BUS_NAME_HPP
