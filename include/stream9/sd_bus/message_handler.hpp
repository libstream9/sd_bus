#ifndef STREAM9_SD_BUS_MESSAGE_HANDLER_HPP
#define STREAM9_SD_BUS_MESSAGE_HANDLER_HPP

#include <functional>

namespace stream9::sd_bus {

class dbus_error;
class message;

using message_handler = std::function<int(message, dbus_error)>;

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_MESSAGE_HANDLER_HPP
