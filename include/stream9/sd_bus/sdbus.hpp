#ifndef STREAM9_SD_BUS_SD_BUS_HPP
#define STREAM9_SD_BUS_SD_BUS_HPP

#include "cstring.hpp"
#include "cstring_array.hpp"
#include "cstring_view.hpp"
#include "duration.hpp"
#include "message_handler.hpp"
#include "namespace.hpp"

#include <optional>

#include <systemd/sd-bus.h>

namespace stream9::sd_bus {

class bus;
class bus_creds;
class call_awaiter;
class call_method_awaiter;
class message;
class method_call;
class method_return;
class slot;

message
call_method(bus&, method_call&, duration timeout = {});

message
call_method(bus&,
            cstring_view destination,
            cstring_view path,
            cstring_view interface,
            cstring_view member,
            cstring_view type, ...);

slot
call_method_async(bus&, method_call&, message_handler,
                  duration timeout = {});

call_awaiter
call_method_async(bus&, method_call&, duration timeout = {});

slot
call_method_async(bus& b,
                  cstring_view destination,
                  cstring_view path,
                  cstring_view interface,
                  cstring_view member,
                  message_handler callback,
                  cstring_view type, ...);

call_method_awaiter
call_method_async(bus&,
                  cstring_view destination,
                  cstring_view path,
                  cstring_view interface,
                  cstring_view member,
                  cstring_view type, ...);

message
get_property(bus&,
             cstring_view destination,
             cstring_view path,
             cstring_view interface,
             cstring_view member,
             cstring_view type);

void
get_property_trivial(bus&,
                     cstring_view destination,
                     cstring_view path,
                     cstring_view interface,
                     cstring_view member,
                     char type, void* ret_ptr);

const_cstring
get_property_string(bus&,
                    cstring_view destination,
                    cstring_view path,
                    cstring_view interface,
                    cstring_view member);

cstring_array
get_property_strv(bus&,
                  cstring_view destination,
                  cstring_view path,
                  cstring_view interface,
                  cstring_view member);

void
set_property(bus&,
             cstring_view destination,
             cstring_view path,
             cstring_view interface,
             cstring_view member,
             cstring_view type, ...);

void
emit_signal(bus&,
            cstring_view path,
            cstring_view interface,
            cstring_view member,
            cstring_view types, ...);

void
emit_properties_changed(bus&,
                        cstring_view path,
                        cstring_view interface,
                        cstring_view name, ...) _sd_sentinel_;

void
emit_properties_changed(bus&,
                        cstring_view path,
                        cstring_view interface,
                        cstring_view names[]);

void
emit_object_added(bus&, cstring_view path);

void
emit_object_removed(bus&, cstring_view path);

void
emit_interfaces_added(bus&,
                      cstring_view path,
                      cstring_view interface, ...) _sd_sentinel_;

void
emit_interfaces_added(bus&, cstring_view path, cstring_view interface[]);

void
emit_interfaces_removed(bus&,
                        cstring_view path,
                        cstring_view interface, ...) _sd_sentinel_;

void
emit_interfaces_removed(bus&, cstring_view path, cstring_view interface[]);

uint64_t
send(bus, message);

uint64_t
send_to(bus&, message&, cstring_view destination);

bus_creds
query_sender_creds(message&, uint64_t mask);

bool
query_sender_privilege(message&, int capability);

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_SD_BUS_HPP
