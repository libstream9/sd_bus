#ifndef STREAM9_SD_BUS_NAMESPACE_HPP
#define STREAM9_SD_BUS_NAMESPACE_HPP

namespace std::ranges {}
namespace stream9::strings {}
namespace stream9::iterators {}
namespace stream9::errors {}

namespace stream9::sd_bus {

namespace rng { using namespace std::ranges; }
namespace str = stream9::strings;
namespace iter = stream9::iterators;
namespace err = stream9::errors;

} // namespace stream9::dbus

#endif // STREAM9_SD_BUS_NAMESPACE_HPP
