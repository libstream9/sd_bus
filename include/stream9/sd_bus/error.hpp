#ifndef STREAM9_SD_BUS_ERROR_HPP
#define STREAM9_SD_BUS_ERROR_HPP

#include "cstring_view.hpp"
#include "namespace.hpp"

#include <source_location>
#include <system_error>

#include <systemd/sd-bus.h>

#include <stream9/linux/error.hpp>

namespace stream9::sd_bus {

class error : public stream9::linux::error
{
public:
    error(std::source_location where = std::source_location::current());

    error(std::string what,
          std::source_location where = std::source_location::current());

    error(std::string what, int negative_errno,
          std::source_location where = std::source_location::current());

    error(std::string what, int negative_errno, std::string context,
          std::source_location where = std::source_location::current());

public:
    enum class errc {
        invalid_slot_data = 1,
        invalid_signature = 2,
    };

    static std::error_category const& category();

    friend std::error_code
    make_error_code(errc const e)
    {
        return { static_cast<int>(e), category() };
    }

};

class dbus_error : public stream9::errors::error
{
public:
    // essential
    dbus_error(std::source_location where = std::source_location::current());
    dbus_error(::sd_bus_error const&, std::source_location where = std::source_location::current());

    ~dbus_error() noexcept;

    dbus_error(dbus_error const&);
    dbus_error& operator=(dbus_error const&);

    dbus_error(dbus_error&&) noexcept;
    dbus_error& operator=(dbus_error&&) noexcept;

    void swap(dbus_error&) noexcept;

    // accessor
    cstring_view name() const noexcept { return m_error.name; }
    cstring_view message() const noexcept { return m_error.message; }

    auto&       base() noexcept { return m_error; }
    auto const& base() const noexcept { return m_error; }

    // query
    bool is_set() const noexcept;
    bool has_name(cstring_view name) const noexcept;
    int get_errno() const;

    char const* what() const noexcept override;

    // modifier
    dbus_error& set(cstring_view name, cstring_view message) &;
    dbus_error&& set(cstring_view name, cstring_view message) &&;

    dbus_error& set_errno(int error) &;
    dbus_error&& set_errno(int error) &&;

    dbus_error& set_errno(int error, cstring_view format, ...) &;
    dbus_error&& set_errno(int error, cstring_view format, ...) &&;

private:
    ::sd_bus_error m_error {};
};

using stream9::errors::internal_error;

} // namespace stream9::sd_bus

namespace std {

template<>
struct is_error_code_enum<stream9::sd_bus::error::errc>
    : true_type {};

template<>
struct is_error_condition_enum<stream9::sd_bus::error::errc>
    : true_type {};

} // namespace std

#if 0
int sd_bus_error_setf(sd_bus_error *e, const char *name, const char *format, ...)  _sd_printf_(3, 4);
int sd_bus_error_set_const(sd_bus_error *e, const char *name, const char *message);
int sd_bus_error_has_names_sentinel(const sd_bus_error *e, ...) _sd_sentinel_;
#define sd_bus_error_has_names(e, ...) sd_bus_error_has_names_sentinel(e, __VA_ARGS__, NULL)

#define SD_BUS_ERROR_MAP(_name, _code)          \
        {                                       \
                .name = _name,                  \
                .code = _code,                  \
        }
#define SD_BUS_ERROR_MAP_END                    \
        {                                       \
                .name = NULL,                   \
                .code = - 'x',                  \
        }

int sd_bus_error_add_map(const sd_bus_error_map *map);
#endif

#endif // STREAM9_SD_BUS_ERROR_HPP
