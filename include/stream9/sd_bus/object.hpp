#ifndef STREAM9_SD_BUS_OBJECT_HPP
#define STREAM9_SD_BUS_OBJECT_HPP

#include "bus.hpp"
#include "cstring_view.hpp"
#include "vtable.hpp"

#include <optional>

#include <systemd/sd-bus.h>

namespace stream9::sd_bus {

class message;

class object
{
public:
    // essential
    object(class bus, cstring_view path, cstring_view interface, vtable&&);

    ~object() noexcept;

    object(object const&) = delete;
    object& operator=(object const&) = delete;

    object(object&&) noexcept;
    object& operator=(object&&) noexcept;

    // query
    class bus                   bus() const noexcept;
    std::optional<cstring_view> description() const;
    bool                        is_floating() const;
    ::sd_bus_destroy_t          destroy_callback() const;

    std::optional<message> current_message() const;

    // modifier
    void set_user_description(cstring_view);
    void set_floating(bool enable);
    void set_destroy_callback(::sd_bus_destroy_t callback);

private:
    ::sd_bus_slot* m_slot {};
    vtable m_vtbl;
};

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_OBJECT_HPP
