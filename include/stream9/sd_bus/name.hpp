#ifndef STREAM9_SD_BUS_NAME_HPP
#define STREAM9_SD_BUS_NAME_HPP

#include <systemd/sd-bus.h>

namespace stream9::sd_bus {

//TODO test
inline bool
is_valid_interface_name(char const* const s)
{
    return ::sd_bus_interface_name_is_valid(s);
}

inline bool
is_valid_service_name(char const* const s)
{
    return ::sd_bus_service_name_is_valid(s);
}

inline bool
is_valid_member_name(char const* const s)
{
    return ::sd_bus_member_name_is_valid(s);
}

inline bool
is_valid_object_path(char const* s)
{
    return ::sd_bus_object_path_is_valid(s);
}

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_NAME_HPP
