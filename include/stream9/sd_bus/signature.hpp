#ifndef STREAM9_SD_BUS_SIGNATURE_HPP
#define STREAM9_SD_BUS_SIGNATURE_HPP

#include "cstring_view.hpp"

#include <string>
#include <string_view>

namespace stream9::sd_bus {

class signature : public cstring_view
{
public:
    signature() = default;

    template<std::size_t N>
    consteval signature(char const (&s)[N]) noexcept;

    constexpr signature(cstring_view);

    constexpr signature(std::string const&);

    explicit constexpr signature(std::string_view const&);
};

class array_contents_signature : public signature
{
public:
    using signature::signature;

    template<std::size_t N>
    consteval array_contents_signature(char const (&s)[N]) noexcept;
};

class struct_contents_signature : public signature
{
public:
    using signature::signature;

    template<std::size_t N>
    consteval struct_contents_signature(char const (&s)[N]) noexcept;
};

class variant_contents_signature : public signature
{
public:
    using signature::signature;

    template<std::size_t N>
    consteval variant_contents_signature(char const (&s)[N]) noexcept;
};

class dict_entry_contents_signature : public signature
{
public:
    using signature::signature;

    template<std::size_t N>
    consteval dict_entry_contents_signature(char const (&s)[N]) noexcept;
};

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_SIGNATURE_HPP

#include "signature.ipp"
