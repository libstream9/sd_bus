#ifndef STREAM9_SD_BUS_PROPERTY_HANDLER_HPP
#define STREAM9_SD_BUS_PROPERTY_HANDLER_HPP

#include "cstring_view.hpp"

#include <functional>

namespace stream9::sd_bus {

class bus;
class message;
class dbus_error;

using property_handler = std::function<
    int(bus, cstring_view path, cstring_view interface, cstring_view property,
        message, dbus_error) >;

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_PROPERTY_HANDLER_HPP
