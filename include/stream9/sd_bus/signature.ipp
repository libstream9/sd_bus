#ifndef STREAM9_SD_BUS_SIGNATURE_IPP
#define STREAM9_SD_BUS_SIGNATURE_IPP

#include "signature.hpp"

#include "error.hpp"

#include <systemd/sd-bus.h>

namespace stream9::sd_bus {

namespace signature_ {

    inline constexpr bool
    is_basic_type(char const t)
    {
        switch (t) {
            case SD_BUS_TYPE_BYTE:
            case SD_BUS_TYPE_BOOLEAN:
            case SD_BUS_TYPE_INT16:
            case SD_BUS_TYPE_UINT16:
            case SD_BUS_TYPE_INT32:
            case SD_BUS_TYPE_UINT32:
            case SD_BUS_TYPE_INT64:
            case SD_BUS_TYPE_UINT64:
            case SD_BUS_TYPE_DOUBLE:
            case SD_BUS_TYPE_STRING:
            case SD_BUS_TYPE_OBJECT_PATH:
            case SD_BUS_TYPE_SIGNATURE:
            case SD_BUS_TYPE_UNIX_FD:
                return true;
        }

        return false;
    }

    inline constexpr bool
    basic_type(auto& i, auto const e)
    {
        (void)e;

        if (is_basic_type(*i)) {
            ++i;
            return true;
        }

        return false;
    }

    inline constexpr bool
    variant(auto& i, auto const e)
    {
        (void)e;

        if (*i == SD_BUS_TYPE_VARIANT) {
            ++i;
            return true;
        }
        return false;
    }

    inline constexpr bool single_complete_type(auto& i, auto e);

    inline constexpr bool
    dict_entry_contents(auto& i, auto const e)
    {
        if (!basic_type(i, e)) return false; // key
        if (!single_complete_type(i, e)) return false; // value

        return true;
    }

    inline constexpr bool
    dict_entry(auto& i, auto const e)
    {
        if (*i != SD_BUS_TYPE_DICT_ENTRY_BEGIN) return false;
        ++i;

        if (!dict_entry_contents(i, e)) return false;

        if (*i == SD_BUS_TYPE_DICT_ENTRY_END) {
            ++i;
            return true;
        }

        return false;
    }

    inline constexpr bool
    array_contents(auto& i, auto const e)
    {
        return dict_entry(i, e) || single_complete_type(i, e);
    }

    inline constexpr bool
    array(auto& i, auto const e)
    {
        if (*i == SD_BUS_TYPE_ARRAY) {
            ++i;
            return array_contents(i, e);
        }

        return false;
    }

    inline constexpr bool
    structure(auto& i, auto const e)
    {
        if (*i != SD_BUS_TYPE_STRUCT_BEGIN) return false;

        ++i;
        if (*i == SD_BUS_TYPE_STRUCT_END) return false;

        while (i != e) {
            if (*i == SD_BUS_TYPE_STRUCT_END) {
                ++i;
                return true;
            }
            if (!single_complete_type(i, e)) {
                return false;
            }
        }

        return false;
    }

    inline constexpr bool
    single_complete_type(auto& i, auto const e)
    {
        if (basic_type(i, e)) {
            return true;
        }
        else if (variant(i, e)) {
            return true;
        }
        else if (array(i, e)) {
            return true;
        }
        else if (structure(i, e)) {
            return true;
        }

        return false;
    }

} // namespace signature_

inline constexpr bool
is_valid_signature(cstring_view const s)
{
    using namespace signature_;

    if (s.empty()) return false;

    auto i = s.begin();
    auto const e = s.end();

    while (i != e) {
        if (!single_complete_type(i, e)) return false;
    }

    return true;
}

inline constexpr bool
is_valid_array_contents_signature(cstring_view const s)
{
    using namespace signature_;

    if (s.empty()) return false;

    auto i = s.begin();
    auto const e = s.end();

    if (array_contents(i, e)) {
        return i == e;
    }

    return false;
}

inline constexpr bool
is_valid_variant_contents_signature(cstring_view const s)
{
    using namespace signature_;

    if (s.empty()) return false;

    auto i = s.begin();
    auto const e = s.end();

    if (single_complete_type(i, e)) {
        return i == e;
    }

    return false;
}

inline constexpr bool
is_valid_dict_entry_contents_signature(cstring_view const s)
{
    using namespace signature_;

    if (s.empty()) return false;

    auto i = s.begin();
    auto const e = s.end();

    if (!dict_entry_contents(i, e)) return false;

    return i == e;
}

template<std::size_t N>
consteval signature::
signature(char const (&s)[N]) noexcept
    : cstring_view { s }
{
    if (!is_valid_signature(*this)) {
        throw error().why(error::errc::invalid_signature);
    }
}

constexpr signature::
signature(cstring_view const s)
    : cstring_view { s }
{}

constexpr signature::
signature(std::string const& s)
    : cstring_view { s }
{}

constexpr signature::
signature(std::string_view const& s)
    : cstring_view { s }
{}

template<std::size_t N>
consteval array_contents_signature::
array_contents_signature(char const (&s)[N]) noexcept
    : signature { cstring_view(s) }
{
    if (!is_valid_array_contents_signature(*this)) {
        throw error().why(error::errc::invalid_signature);
    }
}

template<std::size_t N>
consteval struct_contents_signature::
struct_contents_signature(char const (&s)[N]) noexcept
    : signature { cstring_view(s) }
{
    if (!is_valid_signature(*this)) {
        throw error().why(error::errc::invalid_signature);
    }
}

template<std::size_t N>
consteval variant_contents_signature::
variant_contents_signature(char const (&s)[N]) noexcept
    : signature { cstring_view(s) }
{
    if (!is_valid_variant_contents_signature(*this)) {
        throw error().why(error::errc::invalid_signature);
    }
}

template<std::size_t N>
consteval dict_entry_contents_signature::
dict_entry_contents_signature(char const (&s)[N]) noexcept
    : signature { cstring_view(s) }
{
    if (!is_valid_dict_entry_contents_signature(*this)) {
        throw error().why(error::errc::invalid_signature);
    }
}



} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_SIGNATURE_IPP
