#include <stream9/sd_bus/awaiter.hpp>

namespace stream9::sd_bus {

/*
 * class message_awaiter
 */
message_awaiter::
~message_awaiter() noexcept
{
    if (m_slot) {
        ::sd_bus_slot_unref(*m_slot);
    }
}

message_awaiter::
message_awaiter(message_awaiter const& other) noexcept
    : m_handle { other.m_handle }
    , m_slot { other.m_slot } //TODO maybe split slot into base and one with custom userdata
    , m_result { other.m_result }
    , m_error { other.m_error }
    , m_loc { other.m_loc }
{
    if (m_slot) {
        ::sd_bus_slot_ref(*m_slot);
    }
}

message_awaiter& message_awaiter::
operator=(message_awaiter const& other) noexcept
{
    message_awaiter tmp { other };
    swap(tmp);

    return *this;
}

message_awaiter::
message_awaiter(message_awaiter&& other) noexcept
{
    swap(other);
}

message_awaiter& message_awaiter::
operator=(message_awaiter&& other) noexcept
{
    swap(other);

    return *this;
}

void message_awaiter::
swap(message_awaiter& other) noexcept
{
    using std::swap;
    swap(m_handle, other.m_handle);
    swap(m_slot, other.m_slot);
    swap(m_result, other.m_result);
    swap(m_error, other.m_error);
    swap(m_loc, other.m_loc);
}

method_return message_awaiter::
await_resume() const
{
    if (m_error) {
        throw *m_error;
    }
    else {
        assert(m_result);
        return *m_result;
    }
}

void message_awaiter::
set_handle(std::coroutine_handle<> h)
{
    m_handle = h;
}

void message_awaiter::
set_slot(::sd_bus_slot* s)
{
    m_slot.emplace(s);
}

void message_awaiter::
set_location(std::source_location l)
{
    m_loc = std::move(l);
}

int message_awaiter::
callback(::sd_bus_message* const m,
         void* const userdata, ::sd_bus_error*)
{
    assert(m);

    auto self = static_cast<message_awaiter*>(userdata);
    assert(self);

    if (auto* const e = ::sd_bus_message_get_error(m)) {
        self->m_error.emplace(*e, self->m_loc);
    }
    else {
        self->m_result.emplace(*m);
    }

    self->m_handle.resume();

    return 0;
}

/*
 * class call_awaiter
 */
call_awaiter::
call_awaiter(bus& b, method_call& m, duration const timeout)
    : m_bus { b }
    , m_message { m }
    , m_usec { timeout.count() }
{}

void call_awaiter::
await_suspend(std::coroutine_handle<> h, std::source_location l/*= current*/)
{
    this->set_handle(h);
    this->set_location(l);

    ::sd_bus_slot* slot = nullptr;

    auto const rv = ::sd_bus_call_async(m_bus, &slot, m_message,
                        callback, static_cast<message_awaiter*>(this), m_usec );
    if (rv < 0) {
        throw sd_bus::error { "sd_bus_call_async", rv };
    }

    assert(slot);
    this->set_slot(slot);
}

/*
 * calss call_method_awaiter
 */
call_method_awaiter::
call_method_awaiter(bus& b,
                    cstring_view const destination,
                    cstring_view const path,
                    cstring_view const interface,
                    cstring_view const member,
                    cstring_view const type, va_list va)
{
    ::sd_bus_slot* slot = nullptr;

    auto const rv = ::sd_bus_call_method_asyncv(b, &slot,
        destination, path, interface, member,
        callback, static_cast<message_awaiter*>(this),
        type, va );
    if (rv < 0) {
        throw sd_bus::error { "sd_bus_call_method_asyncv", rv };
    }

    assert(slot);
    this->set_slot(slot);
}

void call_method_awaiter::
await_suspend(std::coroutine_handle<> h, std::source_location l)
{
    this->set_handle(h);
    this->set_location(std::move(l));
}

} // namespace stream9::sd_bus
