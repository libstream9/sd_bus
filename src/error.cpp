#include <stream9/sd_bus/error.hpp>

#include <cassert>

namespace stream9::sd_bus {

/*
 * class error
 */
error::
error(std::source_location where/* = std::source_location::current()*/)
    : stream9::linux::error { std::move(where) }
{}

error::
error(std::string what,
      std::source_location where/* = std::source_location::current()*/)
    : stream9::linux::error { std::move(what), std::move(where) }
{}

error::
error(std::string what, int const negative_errno,
      std::source_location where/* = std::source_location::current()*/)
    : stream9::linux::error { std::move(what), -negative_errno, std::move(where) }
{
    assert(negative_errno < 0);
}

error::
error(std::string what, int const negative_errno, std::string context,
      std::source_location where /* = std::source_location::current()*/)
    : stream9::linux::error {
        std::move(what), -negative_errno, std::move(context), std::move(where) }
{
    assert(negative_errno < 0);
}

std::error_category const& error::
category()
{
    static struct impl : std::error_category
    {
        char const* name() const noexcept override
        {
            return "stream9::sd_bus::error";
        }

        std::string message(int const ec) const override
        {
            using enum errc;

            switch (static_cast<errc>(ec)) {
                case invalid_slot_data:
                    return "invalid slot data";
                case invalid_signature:
                    return "invalid signature";
            }

            return "unknown error";
        }
    } instance;

    return instance;
}

/*
 * dbus_error
 */
dbus_error::
dbus_error(std::source_location where)
    : error { std::move(where) }
{}

dbus_error::
dbus_error(::sd_bus_error const& e, std::source_location where)
    : error { e.message ? e.message : std::string(), std::move(where) }
    , m_error { e }
{}

dbus_error::
~dbus_error() noexcept
{
    ::sd_bus_error_free(&m_error);
}

dbus_error::
dbus_error(dbus_error const& other)
    : error { other }
{
    ::sd_bus_error_copy(&m_error, &other.m_error);
}

dbus_error& dbus_error::
operator=(dbus_error const& other)
{
    dbus_error tmp { other };
    swap(tmp);

    return *this;
}

dbus_error::
dbus_error(dbus_error&& other) noexcept
    : error { std::move(other) }
{
    ::sd_bus_error_move(&m_error, &other.m_error);
}

dbus_error& dbus_error::
operator=(dbus_error&& other) noexcept
{
    dbus_error tmp { std::move(other) };
    swap(tmp);

    return *this;
}

void dbus_error::
swap(dbus_error& other) noexcept
{
    using std::swap;
    swap(static_cast<error&>(*this), static_cast<error&>(other));
    swap(m_error, other.m_error);
}

/*
 * query
 */
bool dbus_error::
is_set() const noexcept
{
    return ::sd_bus_error_is_set(&m_error);
}

bool dbus_error::
has_name(str::cstring_view const name) const noexcept
{
    return ::sd_bus_error_has_name(&m_error, name);
}

int dbus_error::
get_errno() const
{
    auto const rv = ::sd_bus_error_get_errno(&m_error);
    if (rv < 0) {
        throw sd_bus::error { "sd_bus_error_get_errno", rv };
    }

    return rv;
}

char const* dbus_error::
what() const noexcept
{
    return m_error.message;
}

dbus_error& dbus_error::
set(cstring_view const name, cstring_view const message) &
{
    ::sd_bus_error_set(&m_error, name, message);

    return *this;
}

dbus_error&& dbus_error::
set(cstring_view const name, cstring_view const message) &&
{
    return static_cast<dbus_error&&>(
        static_cast<dbus_error&>(*this).set(name, message) );
}

dbus_error& dbus_error::
set_errno(int const e) &
{
    ::sd_bus_error_set_errno(&m_error, e);

    return *this;
}

dbus_error&& dbus_error::
set_errno(int const error) &&
{
    return static_cast<dbus_error&&>(
        static_cast<dbus_error&>(*this).set_errno(error) );
}

dbus_error& dbus_error::
set_errno(int const e, cstring_view const format, ...) &
{
    va_list va;

    va_start(va, format);
    ::sd_bus_error_set_errnofv(&m_error, e, format, va);
    va_end(va);

    return *this;
}

dbus_error&& dbus_error::
set_errno(int const e, cstring_view const format, ...) &&
{
    va_list va;

    va_start(va, format);
    ::sd_bus_error_set_errnofv(&m_error, e, format, va);
    va_end(va);

    return std::move(*this);
}

} // namespace stream9::sd_bus
