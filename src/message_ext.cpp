#include <stream9/sd_bus/message_ext.hpp>

namespace stream9::sd_bus {

template<typename V, char T>
static V
read_basic(message& m)
{
    V result;

    m.read_basic(T, &result);

    return result;
}

uint8_t
read_byte(message& m)
{
    try {
        return read_basic<uint8_t, SD_BUS_TYPE_BYTE>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

bool
read_boolean(message& m)
{
    try {
        return read_basic<int, SD_BUS_TYPE_BOOLEAN>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

int16_t
read_int16(message& m)
{
    try {
        return read_basic<int16_t, SD_BUS_TYPE_INT16>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

uint16_t
read_uint16(message& m)
{
    try {
        return read_basic<uint16_t, SD_BUS_TYPE_UINT16>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

int32_t
read_int32(message& m)
{
    try {
        return read_basic<int32_t, SD_BUS_TYPE_INT32>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

uint32_t
read_uint32(message& m)
{
    try {
        return read_basic<uint32_t, SD_BUS_TYPE_UINT32>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

int64_t
read_int64(message& m)
{
    try {
        return read_basic<int64_t, SD_BUS_TYPE_INT64>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

uint64_t
read_uint64(message& m)
{
    try {
        return read_basic<uint64_t, SD_BUS_TYPE_UINT64>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

double
read_double(message& m)
{
    try {
        return read_basic<double, SD_BUS_TYPE_DOUBLE>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

cstring_view
read_string(message& m)
{
    try {
        return read_basic<char const*, SD_BUS_TYPE_STRING>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

cstring_view
read_object_path(message& m)
{
    try {
        return read_basic<char const*, SD_BUS_TYPE_OBJECT_PATH>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

cstring_view
read_signature(message& m)
{
    try {
        return read_basic<char const*, SD_BUS_TYPE_SIGNATURE>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

int
read_unix_fd(message& m)
{
    try {
        return read_basic<int, SD_BUS_TYPE_UNIX_FD>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

template<typename V, char T>
static std::span<V const>
read_array(message& m)
{
    void const* p {};
    std::size_t sz {};

    m.read_array(T, &p, &sz);

    return {
        static_cast<V const*>(p),
        sz / alignof(V)
    };
}

std::span<uint8_t const>
read_byte_array(message& m)
{
    try {
        return read_array<uint8_t, SD_BUS_TYPE_BYTE>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

std::span<int const>
read_boolean_array(message& m)
{
    try {
        return read_array<int, SD_BUS_TYPE_BOOLEAN>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

std::span<int16_t const>
read_int16_array(message& m)
{
    try {
        return read_array<int16_t, SD_BUS_TYPE_INT16>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

std::span<uint16_t const>
read_uint16_array(message& m)
{
    try {
        return read_array<uint16_t, SD_BUS_TYPE_UINT16>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

std::span<int32_t const>
read_int32_array(message& m)
{
    try {
        return read_array<int32_t, SD_BUS_TYPE_INT32>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

std::span<uint32_t const>
read_uint32_array(message& m)
{
    try {
        return read_array<uint32_t, SD_BUS_TYPE_UINT32>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

std::span<int64_t const>
read_int64_array(message& m)
{
    try {
        return read_array<int64_t, SD_BUS_TYPE_INT64>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

std::span<uint64_t const>
read_uint64_array(message& m)
{
    try {
        return read_array<uint64_t, SD_BUS_TYPE_UINT64>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

std::span<double const>
read_double_array(message& m)
{
    try {
        return read_array<double, SD_BUS_TYPE_DOUBLE>(m);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

void
append_byte(message& m, uint8_t const v)
{
    try {
        m.append_basic(SD_BUS_TYPE_BYTE, &v);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

void
append_boolean(message& m, bool const v)
{
    try {
        int const i = v;
        m.append_basic(SD_BUS_TYPE_BOOLEAN, &i);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

void
append_int16(message& m, int16_t const v)
{
    try {
        m.append_basic(SD_BUS_TYPE_INT16, &v);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

void
append_uint16(message& m, uint16_t const v)
{
    try {
        m.append_basic(SD_BUS_TYPE_UINT16, &v);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

void
append_int32(message& m, int32_t const v)
{
    try {
        m.append_basic(SD_BUS_TYPE_INT32, &v);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

void
append_uint32(message& m, uint32_t const v)
{
    try {
        m.append_basic(SD_BUS_TYPE_UINT32, &v);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

void
append_int64(message& m, int64_t const v)
{
    try {
        m.append_basic(SD_BUS_TYPE_INT64, &v);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

void
append_uint64(message& m, uint64_t const v)
{
    try {
        m.append_basic(SD_BUS_TYPE_UINT64, &v);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

void
append_double(message& m, double const v)
{
    try {
        m.append_basic(SD_BUS_TYPE_DOUBLE, &v);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

void
append_string(message& m, cstring_view const v)
{
    try {
        m.append_basic(SD_BUS_TYPE_STRING, v.data());
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

void
append_object_path(message& m, cstring_view const v)
{
    try {
        m.append_basic(SD_BUS_TYPE_OBJECT_PATH, v.data());
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

void
append_signature(message& m, cstring_view const v)
{
    try {
        m.append_basic(SD_BUS_TYPE_SIGNATURE, v.data());
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

void
append_unix_fd(message& m, int const v)
{
    try {
        m.append_basic(SD_BUS_TYPE_UNIX_FD, &v);
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

} // namespace stream9::sd_bus
