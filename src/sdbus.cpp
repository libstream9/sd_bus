#include <stream9/sd_bus/sdbus.hpp>

#include "slot_p.hpp"

#include <stream9/sd_bus/awaiter.hpp>
#include <stream9/sd_bus/bus.hpp>
#include <stream9/sd_bus/bus_creds.hpp>
#include <stream9/sd_bus/error.hpp>
#include <stream9/sd_bus/message.hpp>
#include <stream9/sd_bus/slot.hpp>

namespace stream9::sd_bus {

message
call_method(bus& b, method_call& m, duration const timeout/*= {}*/)
{
    ::sd_bus_message* reply = nullptr;
    dbus_error e;

    auto const rv = ::sd_bus_call(b, m, timeout.count(), &e.base(), &reply);

    if (rv < 0) {
        throw e;
    }

    assert(reply);
    return *reply;
}

message
call_method(bus& b, str::cstring_view const destination,
            str::cstring_view const path,
            str::cstring_view const interface,
            str::cstring_view const member,
            str::cstring_view const type, ...)
{
    ::sd_bus_message* reply = nullptr;
    dbus_error e;

    va_list va;
    va_start(va, type);

    auto const rv = ::sd_bus_call_methodv(b,
        destination, path, interface, member,
        &e.base(), &reply,
        type, va
    );

    va_end(va);

    if (rv < 0) {
        throw e;
    }

    assert(reply);
    return *reply;
}

slot
call_method_async(bus& b, method_call& m, message_handler callback,
                  duration const timeout/*= {}*/)
{
    try {
        ::sd_bus_slot* result {};

        auto data = std::make_unique<slot_data>();
        data->message_callback = std::move(callback);

        auto const rc = ::sd_bus_call_async(
                        b, &result, m, on_message, data.get(), timeout.count());
        if (rc < 0) {
            throw error { "sd_bus_call_async", rc };
        }

        assert(result);
        return { *result, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

call_awaiter
call_method_async(bus& b, method_call& m, duration timeout/*= 0*/)
{
    return { b, m, timeout };
}

slot
call_method_async(bus& b,
                  cstring_view const destination,
                  cstring_view const path,
                  cstring_view const interface,
                  cstring_view const member,
                  message_handler callback,
                  cstring_view const type, ...)
{
    try {
        ::sd_bus_slot* result = nullptr;
        va_list va;

        auto data = std::make_unique<slot_data>();
        data->message_callback = std::move(callback);

        va_start(va, type);
        auto const rv = ::sd_bus_call_method_asyncv(b, &result,
            destination, path, interface, member,
            on_message, data.get(), type, va
        );
        va_end(va);

        if (rv < 0) {
            throw error { "sd_bus_call_method_asyncv", rv };
        }

        assert(result);
        return { *result, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(internal_error());
    }
}

call_method_awaiter
call_method_async(bus& b,
                  cstring_view destination,
                  cstring_view path,
                  cstring_view interface,
                  cstring_view member,
                  cstring_view type, ...)
{
    va_list va;

    va_start(va, type);
    call_method_awaiter result {
        b, destination, path, interface, member, type, va
    };
    va_end(va);

    return result;
}

message
get_property(bus& b, str::cstring_view const destination,
             str::cstring_view const path,
             str::cstring_view const interface,
             str::cstring_view const member,
             str::cstring_view const type)
{
    ::sd_bus_message* reply = nullptr;
    dbus_error e;

    auto const rv = ::sd_bus_get_property(b,
        destination, path, interface, member,
        &e.base(), &reply,
        type
    );

    if (rv < 0) {
        throw e;
    }

    assert(reply);
    return *reply;
}

void
get_property_trivial(bus& b,
                     str::cstring_view const destination,
                     str::cstring_view const path,
                     str::cstring_view const interface,
                     str::cstring_view const member,
                     char const type,
                     void* const ret_ptr)
{
    dbus_error e;

    auto const rv = ::sd_bus_get_property_trivial(b,
        destination, path, interface, member,
        &e.base(), type, ret_ptr );

    if (rv < 0) {
        throw e;
    }
}

const_cstring
get_property_string(bus& b,
                    str::cstring_view const destination,
                    str::cstring_view const path,
                    str::cstring_view const interface,
                    str::cstring_view const member)
{
    dbus_error e;
    char* result = nullptr;

    auto const rv = ::sd_bus_get_property_string(b,
        destination, path, interface, member,
        &e.base(), &result );

    if (rv < 0) {
        throw e;
    }

    return const_cstring { result };
}

cstring_array
get_property_strv(bus& b,
                 str::cstring_view const destination,
                 str::cstring_view const path,
                 str::cstring_view const interface,
                 str::cstring_view const member)
{
    dbus_error e;
    char** result = nullptr;

    auto const rv = ::sd_bus_get_property_strv(b,
        destination, path, interface, member,
        &e.base(), &result );

    if (rv < 0) {
        throw e;
    }

    return cstring_array {
        reinterpret_cast<const_cstring*>(result)
    };
}

void
set_property(bus& b,
             str::cstring_view const destination,
             str::cstring_view const path,
             str::cstring_view const interface,
             str::cstring_view const member,
             str::cstring_view const type, ...)
{
    dbus_error e;

    va_list va;
    va_start(va, type);

    auto const rv = ::sd_bus_set_propertyv(b,
        destination, path, interface, member,
        &e.base(),
        type, va
    );

    va_end(va);

    if (rv < 0) {
        throw e;
    }
}

void
emit_signal(bus& b,
            cstring_view const path,
            cstring_view const interface,
            cstring_view const member,
            cstring_view const types, ...)
{
    va_list va;

    va_start(va, types);
    auto const rv =
        ::sd_bus_emit_signalv(b, path, interface, member, types, va);
    va_end(va);

    if (rv < 0) {
        throw error { "sd_bus_emit_signalv", rv };
    }
}

void
emit_properties_changed(bus& b,
                        cstring_view const path,
                        cstring_view const interface,
                        cstring_view const name, ...)
{
    va_list va;

    va_start(va, name);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat"
    auto const rv =
        ::sd_bus_emit_properties_changed(b, path, interface, name, va);
#pragma GCC diagnostic pop
    va_end(va);

    if (rv < 0) {
        throw error { "sd_bus_emit_properties_changed", rv };
    }
}

void
emit_properties_changed(bus& b,
                        cstring_view const path,
                        cstring_view const interface,
                        cstring_view names[])
{
    auto const rv = ::sd_bus_emit_properties_changed_strv(
                    b, path, interface, reinterpret_cast<char**>(names));
    if (rv < 0) {
        throw error { "sd_bus_emit_properties_changed_strv", rv };
    }
}

void
emit_object_added(bus& b, cstring_view const path)
{
    auto const rv = ::sd_bus_emit_object_added(b, path);
    if (rv < 0) {
        throw error { "sd_bus_emit_object_added", rv };
    }
}

void
emit_object_removed(bus& b, cstring_view const path)
{
    auto const rv = ::sd_bus_emit_object_removed(b, path);
    if (rv < 0) {
        throw error { "sd_bus_emit_object_removed", rv };
    }
}

void
emit_interfaces_added(bus& b,
                      cstring_view const path,
                      cstring_view const interface, ...)
{
    va_list va;

    va_start(va, interface);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat"
    auto const rv =
        ::sd_bus_emit_interfaces_added(b, path, interface, va);
#pragma GCC diagnostic pop
    va_end(va);

    if (rv < 0) {
        throw error { "sd_bus_emit_interfaces_added", rv };
    }
}

void
emit_interfaces_added(bus& b,
                      cstring_view const path,
                      cstring_view interfaces[])
{
    auto const rv = ::sd_bus_emit_interfaces_added_strv(
                         b, path, reinterpret_cast<char**>(interfaces));
    if (rv < 0) {
        throw error { "sd_bus_emit_interfaces_added_strv", rv };
    }
}

void
emit_interfaces_removed(bus& b,
                        cstring_view const path,
                        cstring_view const interface, ...)
{
    va_list va;

    va_start(va, interface);
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wformat"
    auto const rv =
        ::sd_bus_emit_interfaces_removed(b, path, interface, va);
#pragma GCC diagnostic pop
    va_end(va);

    if (rv < 0) {
        throw error { "sd_bus_emit_interfaces_removed", rv };
    }
}

void
emit_interfaces_removed(bus& b,
                        cstring_view const path,
                        cstring_view interfaces[])
{
    auto const rv = ::sd_bus_emit_interfaces_removed_strv(
                         b, path, reinterpret_cast<char**>(interfaces));
    if (rv < 0) {
        throw error { "sd_bus_emit_interfaces_removed_strv", rv };
    }
}

uint64_t
send(bus b, message m)
{
    uint64_t cookie = 0;

    auto const rv = ::sd_bus_send(b, m, &cookie);
    if (rv < 0) {
        throw error { "sd_bus_send", rv };
    }

    return cookie;
}

uint64_t
send_to(bus& b, message& m, cstring_view const destination)
{
    uint64_t cookie = 0;

    auto const rv = ::sd_bus_send_to(b, m, destination, &cookie);
    if (rv < 0) {
        throw error { "sd_bus_send_to", rv };
    }

    return cookie;
}

bus_creds
query_sender_creds(message& m, uint64_t const mask)
{
    ::sd_bus_creds* result = nullptr;

    auto const rv = ::sd_bus_query_sender_creds(m, mask, &result);
    if (rv < 0) {
        throw error { "sd_bus_query_sender_creds", rv };
    }

    assert(result);
    return *result;
}

bool
query_sender_privilege(message& m, int const capability)
{
    auto const rv = ::sd_bus_query_sender_privilege(m, capability);
    if (rv < 0) {
        throw error { "sd_bus_query_sender_privilege", rv };
    }

    return rv;
}

} // namespace stream9::sd_bus
