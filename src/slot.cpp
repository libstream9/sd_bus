#include <stream9/sd_bus/slot.hpp>

#include "slot_p.hpp"

#include <stream9/sd_bus/bus.hpp>
#include <stream9/sd_bus/error.hpp>
#include <stream9/sd_bus/message.hpp>

#include <cassert>
#include <utility>

namespace stream9::sd_bus {

static slot_data*
get_data(::sd_bus_slot* const s)
{
    auto* const userdata = ::sd_bus_slot_get_userdata(s);

    auto* const d = static_cast<slot_data*>(userdata);
    if (!d || !d->validate()) {
        return nullptr;
    }

    return d;
}

static void
on_destroy(void* const ptr)
{
    auto* const data = static_cast<slot_data*>(ptr);
    delete data;
}

/*
 * class slot
 */
slot::
slot(::sd_bus_slot& s)
    : m_handle { &s }
{
    assert(m_handle);

    slot_data::validate(::sd_bus_slot_get_userdata(m_handle));
}

slot::
slot(::sd_bus_slot& s, std::unique_ptr<slot_data> d)
    : m_handle { &s }
{
    assert(m_handle);

    auto const rc = ::sd_bus_slot_set_destroy_callback(m_handle, on_destroy);
    if (rc < 0) {
        throw error { "sd_bus_slot_set_destroy_callback", rc };
    }

    d.release();
}

slot::
~slot() noexcept
{
    if (m_handle) {
        ::sd_bus_slot_unref(m_handle);
    }
}

slot::
slot(slot const& other) noexcept
    : m_handle { ::sd_bus_slot_ref(other.m_handle) }
{}

slot& slot::
operator=(slot const& other) noexcept
{
    slot tmp { other };
    swap(tmp);

    return *this;
}

slot::
slot(slot&& other) noexcept
    : m_handle { nullptr }
{
    swap(other);
}

slot& slot::
operator=(slot&& other) noexcept
{
    swap(other);

    return *this;
}

void slot::
swap(slot& other) noexcept
{
    using std::swap;
    swap(m_handle, other.m_handle);
}

class bus slot::
bus() const noexcept
{
    auto b = ::sd_bus_slot_get_bus(m_handle);
    assert(b);

    return *::sd_bus_ref(b);
}

void* slot::
userdata() const noexcept
{
    auto* const d = get_data(m_handle);
    assert(d);

    return d->userdata;
}

std::optional<cstring_view> slot::
description() const
{
    std::optional<cstring_view> result;

    char const* s = nullptr;
    auto const rc = ::sd_bus_slot_get_description(m_handle, &s);
    if (rc < 0) {
        if (rc == -ENXIO) {
            // nop
        }
        else {
            throw error { "sd_bus_slot_get_description", rc };
        }
    }
    else {
        result.emplace(s);
    }

    return result;
}

bool slot::
is_floating() const
{
    auto const rc = ::sd_bus_slot_get_floating(m_handle);
    if (rc < 0) {
        throw error { "sd_bus_slot_get_floating", rc };
    }

    return rc;
}

::sd_bus_destroy_t slot::
destroy_callback() const noexcept
{
    auto* const d = get_data(m_handle);
    assert(d);

    return d->destroy_callback;
}

std::optional<message> slot::
current_message() const
{
    std::optional<message> result;

    auto const m = ::sd_bus_slot_get_current_message(m_handle);
    if (m) {
        result.emplace(*::sd_bus_message_ref(m));
    }

    return result;
}

message_handler const& slot::
current_handler() const noexcept
{
    auto* const d = get_data(m_handle);
    assert(d);

    return d->message_callback;
}

void* slot::
current_userdata() const noexcept
{
    auto* const userdata = ::sd_bus_slot_get_current_userdata(m_handle);

    if (auto* const d = slot_data::cast(userdata)) {
        return d->userdata;
    }
    else {
        return userdata;
    }
}

void* slot::
set_userdata(void* const userdata) noexcept
{
    auto* const d = get_data(m_handle);
    assert(d);

    return std::exchange(d->userdata, userdata);
}

void slot::
set_user_description(cstring_view const description)
{
    auto const rc = ::sd_bus_slot_set_description(m_handle, description);
    if (rc < 0) {
        throw error { "sd_bus_slot_set_description", rc };
    }
}

void slot::
set_floating(bool const enable)
{
    auto const rc = ::sd_bus_slot_set_floating(m_handle, enable);
    if (rc < 0) {
        throw error { "sd_bus_slot_set_floating", rc };
    }
}

void slot::
set_destroy_callback(::sd_bus_destroy_t const callback) noexcept
{
    auto* const d = get_data(m_handle);
    assert(d);

    d->destroy_callback = callback;
}

} // namespace stream9::sd_bus
