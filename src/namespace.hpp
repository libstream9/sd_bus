#ifndef STREAM9_SD_BUS_SRC_NAMESPACE_HPP
#define STREAM9_SD_BUS_SRC_NAMESPACE_HPP

namespace std::ranges {}

namespace stream9::sd_bus {

namespace rng { using namespace std::ranges; }

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_SRC_NAMESPACE_HPP
