#ifndef STREAM9_SD_BUS_SLOT_P_HPP
#define STREAM9_SD_BUS_SLOT_P_HPP

#include <stream9/sd_bus/error.hpp>
#include <stream9/sd_bus/message.hpp>
#include <stream9/sd_bus/message_handler.hpp>

#include <functional>

namespace stream9::sd_bus {

struct slot_data
{
    static constexpr uint32_t signature = 0x256EC3A7;

    uint32_t head = signature;

    message_handler message_callback {};
    void (*destroy_callback)(void*) {};
    void* userdata {};

    uint32_t tail = signature;

    bool validate() const
    {
       return head == signature && tail == signature;
    }

    static slot_data& validate(void* const ptr)
    {
        if (auto* const d = cast(ptr)) {
            return *d;
        }
        else {
            throw error().why(error::errc::invalid_slot_data);
        }
    }

    static slot_data* cast(void* const ptr)
    {
        auto* const d = static_cast<slot_data*>(ptr);
        if (!d || !d->validate()) {
            return nullptr;
        }
        else {
            return d;
        }
    }
};

inline static int
on_message(::sd_bus_message* const m, void* const userdata,
           ::sd_bus_error* const ret_error)
{
    assert(m);
    assert(ret_error);

    auto& data = slot_data::validate(userdata);

    return data.message_callback(*::sd_bus_message_ref(m), *ret_error);
}

} // namespace stream9::sd_bus

#endif // STREAM9_SD_BUS_SLOT_P_HPP
