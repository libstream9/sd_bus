#include <stream9/sd_bus/bus.hpp>

#include "slot_p.hpp"

#include <stream9/sd_bus/bus_creds.hpp>
#include <stream9/sd_bus/error.hpp>
#include <stream9/sd_bus/message.hpp>
#include <stream9/sd_bus/object.hpp>
#include <stream9/sd_bus/slot.hpp>
#include <stream9/sd_bus/vtable.hpp>

#include <stream9/sd_event/event.hpp>
#include <stream9/json.hpp>

namespace stream9::sd_bus {

static ::sd_bus*
new_bus()
{
    ::sd_bus* result = nullptr;
    auto const rv = ::sd_bus_new(&result);
    if (rv < 0) {
        throw error { "sd_bus_new", rv };
    }

    assert(result != nullptr);
    return result;
}

/*
 * class bus
 */
bus::
bus(cstring_view const address)
    : m_handle { new_bus() }
{
    auto const rv = ::sd_bus_set_address(m_handle, address);
    if (rv < 0) {
        throw error { "sd_bus_set_address", rv };
    }
}

bus::
bus(cstring_view const exec_path, char* const* const argv)
    : m_handle { new_bus() }
{
    auto const rv = ::sd_bus_set_exec(m_handle, exec_path, argv);
    if (rv < 0) {
        throw error { "sd_bus_set_exec", rv };
    }
}

bus::
bus(int const input_fd, int const output_fd)
    : m_handle { new_bus() }
{
    auto const rv = ::sd_bus_set_fd(m_handle, input_fd, output_fd);
    if (rv < 0) {
        throw error { "sd_bus_set_fd", rv };
    }
}

bus::
bus(::sd_bus& bus) noexcept
    : m_handle { &bus }
{}

bus::
~bus() noexcept
{
    if (m_handle) {
        ::sd_bus_unref(m_handle);
    }
}

bus::
bus(bus const& other) noexcept
    : m_handle { ::sd_bus_ref(other.m_handle) }
{}

bus& bus::
operator=(bus const& other) noexcept
{
    bus tmp { *::sd_bus_ref(other.m_handle) };
    swap(tmp);

    return *this;
}

bus::
bus(bus&& other) noexcept
    : m_handle { nullptr }
{
    swap(other);
}

bus& bus::
operator=(bus&& other) noexcept
{
    swap(other);

    return *this;
}

void bus::
swap(bus& other) noexcept
{
    using std::swap;
    swap(m_handle, other.m_handle);
}

bool bus::
is_open() const
{
    auto const rv = ::sd_bus_is_open(m_handle);
    if (rv < 0) {
        throw error { "sd_bus_is_open", rv };
    }

    return rv;
}

bool bus::
is_ready() const
{
    auto const rv = ::sd_bus_is_ready(m_handle);
    if (rv < 0) {
        throw error { "sd_bus_is_ready", rv };
    }

    return rv;
}

bool bus::
is_anonymous() const
{
    auto const rv = ::sd_bus_is_anonymous(m_handle);
    if (rv < 0) {
        throw error { "sd_bus_is_anonymous", rv };
    }

    return rv;
}

bool bus::
is_trusted() const
{
    auto const rv = ::sd_bus_is_trusted(m_handle);
    if (rv < 0) {
        throw error { "sd_bus_is_trusted", rv };
    }

    return rv;
}

bool bus::
is_client() const
{
    auto const rv = ::sd_bus_is_bus_client(m_handle);
    if (rv < 0) {
        throw error { "sd_bus_is_bus_client", rv };
    }

    return rv;
}

bool bus::
is_server() const
{
    auto const rv = ::sd_bus_is_server(m_handle);
    if (rv < 0) {
        throw error { "sd_bus_is_server", rv };
    }

    return rv;
}

bool bus::
is_monitor() const
{
    auto const rv = ::sd_bus_is_monitor(m_handle);
    if (rv < 0) {
        throw error { "sd_bus_is_monitor", rv };
    }

    return rv;
}

bool bus::
allow_interactive_authorization() const
{
    auto const rv = ::sd_bus_get_allow_interactive_authorization(m_handle);
    if (rv < 0) {
        throw error { "sd_bus_get_allow_interactive_authorization", rv };
    }

    return rv;
}

bool bus::
signal_connected() const
{
    auto const rv = ::sd_bus_get_connected_signal(m_handle);
    if (rv < 0) {
        throw error { "sd_bus_get_connected_signal", rv };
    }

    return rv;
}

bool bus::
exit_on_disconnect() const
{
    auto const rv = ::sd_bus_get_exit_on_disconnect(m_handle);
    if (rv < 0) {
        throw error { "sd_bus_get_exit_on_disconnect", rv };
    }

    return rv;
}

bool bus::
watch_bind() const
{
    auto const rv = ::sd_bus_get_watch_bind(m_handle);
    if (rv < 0) {
        throw error { "sd_bus_get_watch_bind", rv };
    }

    return rv;
}

bool bus::
can_send(char const type) const
{
    auto const rv = ::sd_bus_can_send(m_handle, type);
    if (rv < 0) {
        throw error { "sd_bus_can_send", rv };
    }

    return rv;
}

int bus::
fd() const
{
    auto const rv = ::sd_bus_get_fd(m_handle);
    if (rv < 0) {
        throw error { "sd_bus_get_fd", rv };
    }

    return rv;
}

int bus::
events() const
{
    auto const rv = ::sd_bus_get_events(m_handle);
    if (rv < 0) {
        throw error { "sd_bus_get_events", rv };
    }

    return rv;
}

pid_t bus::
tid() const
{
    pid_t result = 0;
    auto const rv = ::sd_bus_get_tid(m_handle, &result);
    if (rv < 0) {
        throw error { "sd_bus_get_tid", rv };
    }

    return result;
}

::sd_id128_t bus::
bus_id() const
{
    ::sd_id128_t result;
    auto const rv = ::sd_bus_get_bus_id(m_handle, &result);
    if (rv < 0) {
        throw error { "sd_bus_get_bus_id", rv };
    }

    return result;
}

duration bus::
timeout() const
{
    uint64_t result;
    auto const rv = ::sd_bus_get_timeout(m_handle, &result);
    if (rv < 0) {
        throw error { "sd_bus_get_timeout", rv };
    }

    return duration { result };
}

duration bus::
method_call_timeout() const
{
    uint64_t result;
    auto const rv = ::sd_bus_get_method_call_timeout(m_handle, &result);
    if (rv < 0) {
        throw error { "sd_bus_get_method_call_timeout", rv };
    }

    return duration { result };
}

uint64_t bus::
creds_mask() const
{
    uint64_t result;
    auto const rv = ::sd_bus_get_creds_mask(m_handle, &result);
    if (rv < 0) {
        throw error { "sd_bus_get_creds_mask", rv };
    }

    return result;
}

uint64_t bus::
n_queued_read() const
{
    uint64_t result;
    auto const rv = ::sd_bus_get_n_queued_read(m_handle, &result);
    if (rv < 0) {
        throw error { "sd_bus_get_n_queued_read", rv };
    }

    return result;
}

uint64_t bus::
n_queued_write() const
{
    uint64_t result;
    auto const rv = ::sd_bus_get_n_queued_write(m_handle, &result);
    if (rv < 0) {
        throw error { "sd_bus_get_n_queued_write", rv };
    }

    return result;
}

cstring_view bus::
description() const
{
    char const* result = nullptr;

    auto const rv = ::sd_bus_get_description(m_handle, &result);
    if (rv < 0) {
        throw error { "sd_bus_get_description", rv };
    }

    return result;
}

std::optional<cstring_view> bus::
scope() const
{
    std::optional<cstring_view> result;
    char const* scope = nullptr;

    auto const rv = ::sd_bus_get_scope(m_handle, &scope);
    if (rv >= 0) {
        result.emplace(scope);
    }
    else if (rv == -ENODATA) {
        // nop
    }
    else {
        throw error { "sd_bus_get_scope", rv };
    }

    return result;
}

cstring_view bus::
unique_name() const
{
    char const* result = nullptr;
    auto const rv = ::sd_bus_get_unique_name(m_handle, &result);
    if (rv < 0) {
        throw error { "sd_bus_get_unique_name", rv };
    }

    return result;
}

std::optional<cstring_view> bus::
address() const
{
    std::optional<cstring_view> result;
    char const* address = nullptr;

    auto const rv = ::sd_bus_get_address(m_handle, &address);
    if (rv >= 0) {
        result.emplace(address);
    }
    else if (rv == -ENODATA) {
        // nop
    }
    else {
        throw error { "sd_bus_get_address", rv };
    }

    return result;
}

std::optional<cstring_view> bus::
default_sender() const
{
    std::optional<cstring_view> result;
    char const* sender = nullptr;

    auto const rv = ::sd_bus_get_sender(m_handle, &sender);
    if (rv >= 0) {
        result.emplace(sender);
    }
    else if (rv == -ENODATA) {
        // nop
    }
    else {
        throw error { "sd_bus_get_sender", rv };
    }

    return result;
}

std::optional<class sd_event::event> bus::
event() const
{
    std::optional<class sd_event::event> result;

    auto const e = ::sd_bus_get_event(m_handle);
    if (e) {
        result.emplace(::sd_event_ref(e));
    }

    return result;
}

::sd_bus_message_handler_t bus::
current_handler() const noexcept
{
    return ::sd_bus_get_current_handler(m_handle);
}

std::optional<message> bus::
current_message() const noexcept
{
    std::optional<message> result;

    auto const m = ::sd_bus_get_current_message(m_handle);
    if (m) {
        result.emplace(*::sd_bus_message_ref(m));
    }

    return result;
}

std::optional<slot> bus::
current_slot() const noexcept
{
    std::optional<slot> result;

    auto const s = ::sd_bus_get_current_slot(m_handle);
    if (s) {
        result.emplace(*::sd_bus_slot_ref(s));
    }

    return result;
}

void* bus::
current_userdata() const noexcept
{
    return ::sd_bus_get_current_userdata(m_handle);
}

bus_creds bus::
creds(uint64_t mask) const
{
    ::sd_bus_creds* result = nullptr;
    auto const rv = ::sd_bus_get_owner_creds(m_handle, mask, &result);
    if (rv < 0) {
        throw error { "sd_bus_get_owner_creds", rv };
    }

    assert(result);
    return *result;
}

void bus::
set_anonymous(bool const b)
{
    auto const rv = ::sd_bus_set_anonymous(m_handle, b);
    if (rv < 0) {
        throw error { "sd_bus_set_anonymous", rv };
    }
}

void bus::
set_trusted(bool const b)
{
    auto const rv = ::sd_bus_set_trusted(m_handle, b);
    if (rv < 0) {
        throw error { "sd_bus_set_trusted", rv };
    }
}

void bus::
set_client(bool const enable)
{
    auto const rv = ::sd_bus_set_bus_client(m_handle, enable);
    if (rv < 0) {
        throw error { "sd_bus_set_bus_client", rv };
    }
}

void bus::
set_server(bool const enable, ::sd_id128_t const id)
{
    auto const rv = ::sd_bus_set_server(m_handle, enable, id);
    if (rv < 0) {
        throw error { "sd_bus_set_server", rv };
    }
}

void bus::
set_monitor(bool const enable)
{
    auto const rv = ::sd_bus_set_monitor(m_handle, enable);
    if (rv < 0) {
        throw error { "sd_bus_set_monitor", rv };
    }
}

void bus::
set_allow_interactive_authorization(bool const b)
{
    auto const rv = ::sd_bus_set_allow_interactive_authorization(m_handle, b);
    if (rv < 0) {
        throw error { "sd_bus_set_allow_interactive_authorization", rv };
    }
}

void bus::
set_signal_connected(bool const b)
{
    auto const rv = ::sd_bus_set_connected_signal(m_handle, b);
    if (rv < 0) {
        throw error { "sd_bus_set_connected_signal", rv };
    }
}

void bus::
set_exit_on_disconnect(bool const b)
{
    auto const rv = ::sd_bus_set_exit_on_disconnect(m_handle, b);
    if (rv < 0) {
        throw error { "sd_bus_set_exit_on_disconnect", rv };
    }
}

void bus::
set_watch_bind(bool const b)
{
    auto const rv = ::sd_bus_set_watch_bind(m_handle, b);
    if (rv < 0) {
        throw error { "sd_bus_set_watch_bind", rv };
    }
}

void bus::
set_negotiate_fds(bool const enable)
{
    auto const rv = ::sd_bus_negotiate_fds(m_handle, enable);
    if (rv < 0) {
        throw error { "sd_bus_negotiate_fds", rv };
    }
}

void bus::
set_negotiate_timestamp(bool const enable)
{
    auto const rv = ::sd_bus_negotiate_timestamp(m_handle, enable);
    if (rv < 0) {
        throw error { "sd_bus_negotiate_timestamp", rv };
    }
}

void bus::
set_negotiate_creds(bool const enable, uint64_t const mask)
{
    auto const rv = ::sd_bus_negotiate_creds(m_handle, enable, mask);
    if (rv < 0) {
        throw error { "sd_bus_negotiate_creds", rv };
    }
}

void bus::
set_method_call_timeout(duration const timeout)
{
    auto const rv = ::sd_bus_set_method_call_timeout(m_handle, timeout.count());
    if (rv < 0) {
        throw error { "sd_bus_set_method_call_timeout", rv };
    }
}

void bus::
set_description(cstring_view const s)
{
    auto const rv = ::sd_bus_set_description(m_handle, s);
    if (rv < 0) {
        throw error { "sd_bus_set_description", rv };
    }
}

void bus::
set_default_sender(cstring_view const s)
{
    auto const rv = ::sd_bus_set_sender(m_handle, s);
    if (rv < 0) {
        throw error { "sd_bus_set_sender", rv };
    }
}

slot bus::
add_match(cstring_view const match, message_handler handler)
{
    try {
        ::sd_bus_slot* result = nullptr;

        auto data = std::make_unique<slot_data>();
        data->message_callback = std::move(handler);

        auto const rv =
            ::sd_bus_add_match(m_handle, &result, match, on_message, data.get());
        if (rv < 0) {
            throw error { "sd_bus_add_match", rv };
        }

        assert(result);
        return { *result, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(
            error().why(errors::error::errc::internal_error) );
    }

}

slot bus::
add_match_async(cstring_view const match,
          message_handler callback,
          ::sd_bus_message_handler_t install_callback)
{
    try {
        ::sd_bus_slot* result = nullptr;

        auto data = std::make_unique<slot_data>();
        data->message_callback = std::move(callback);

        auto const rv = ::sd_bus_add_match_async(
                m_handle, &result, match, on_message, install_callback, data.get());
        if (rv < 0) {
            throw error { "sd_bus_add_match_async", rv };
        }

        assert(result);
        return { *result, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(
            error().why(errors::error::errc::internal_error) );
    }
}

slot bus::
match_signal(cstring_view const sender,
             cstring_view const path,
             cstring_view const interface,
             cstring_view const member,
             message_handler callback)
{
    try {
        ::sd_bus_slot* result = nullptr;

        auto data = std::make_unique<slot_data>();
        data->message_callback = std::move(callback);

        auto const rv = ::sd_bus_match_signal(m_handle, &result,
                       sender, path, interface, member, on_message, data.get());
        if (rv < 0) {
            throw error { "sd_bus_match_signal", rv };
        }

        assert(result);
        return { *result, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(
            error().why(errors::error::errc::internal_error) );
    }
}

slot bus::
match_signal_async(cstring_view const sender,
                   cstring_view const path,
                   cstring_view const interface,
                   cstring_view const member,
                   message_handler callback,
                   ::sd_bus_message_handler_t add_callback)
{
    try {
        ::sd_bus_slot* result = nullptr;

        auto data = std::make_unique<slot_data>();
        data->message_callback = std::move(callback);

        auto const rv = ::sd_bus_match_signal_async(m_handle, &result,
                             sender, path, interface, member,
                             on_message, add_callback, data.get());
        if (rv < 0) {
            throw error { "sd_bus_match_signal_async", rv };
        }

        assert(result);
        return { *result, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(
            error().why(errors::error::errc::internal_error) );
    }
}

slot bus::
add_filter(message_handler callback)
{
    try {
        ::sd_bus_slot* result = nullptr;

        auto data = std::make_unique<slot_data>();
        data->message_callback = std::move(callback);

        auto const rv =
                 ::sd_bus_add_filter(m_handle, &result, on_message, data.get());
        if (rv < 0) {
            throw error { "sd_bus_add_filter", rv };
        }

        assert(result);
        return { *result, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(
            error().why(errors::error::errc::internal_error) );
    }
}

slot bus::
add_object(cstring_view const path, message_handler callback)
{
    try {
        ::sd_bus_slot* result = nullptr;

        auto data = std::make_unique<slot_data>();
        data->message_callback = std::move(callback);

        auto const rv = ::sd_bus_add_object(
                               m_handle, &result, path, on_message, data.get());
        if (rv < 0) {
            throw error { "sd_bus_add_object", rv };
        }

        assert(result);
        return { *result, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(
            error().why(errors::error::errc::internal_error) );
    }

}

object bus::
add_object(cstring_view const path,
           cstring_view const interface,
           vtable&& vtbl)
{
    return { *this, path, interface, std::move(vtbl) };
}

slot bus::
add_fallback(cstring_view const prefix, message_handler callback)
{
    try {
        ::sd_bus_slot* result = nullptr;

        auto data = std::make_unique<slot_data>();
        data->message_callback = std::move(callback);

        auto const rv = ::sd_bus_add_fallback(
                             m_handle, &result, prefix, on_message, data.get());
        if (rv < 0) {
            throw error { "sd_bus_add_fallback", rv };
        }

        assert(result);
        return { *result, std::move(data) };
    }
    catch (...) {
        std::throw_with_nested(
            error().why(errors::error::errc::internal_error) );
    }
}

::sd_bus_slot* bus::
add_fallback(cstring_view const prefix, cstring_view const interface,
             ::sd_bus_vtable const vtbl[],
             ::sd_bus_object_find_t find,
             void* const userdata)
{
    ::sd_bus_slot* result = nullptr;

    auto const rv = ::sd_bus_add_fallback_vtable(
                  m_handle, &result, prefix, interface, vtbl, find, userdata);
    if (rv < 0) {
        throw error { "sd_bus_add_fallback_vtable", rv };
    }

    assert(result);
    return result;
}

::sd_bus_slot* bus::
add_node_enumerator(cstring_view const path,
                    ::sd_bus_node_enumerator_t callback, void* const userdata)
{
    ::sd_bus_slot* result = nullptr;

    auto const rv = ::sd_bus_add_node_enumerator(
                            m_handle, &result, path, callback, userdata);
    if (rv < 0) {
        throw error { "sd_bus_add_node_enumerator", rv };
    }

    assert(result);
    return result;
}

::sd_bus_slot* bus::
add_object_manager(cstring_view const path)
{
    ::sd_bus_slot* result = nullptr;

    auto const rv = ::sd_bus_add_object_manager(m_handle, &result, path);
    if (rv < 0) {
        throw error { "sd_bus_add_object_manager", rv };
    }

    assert(result);
    return result;
}

void bus::
attach_event(class sd_event::event& e, int const priority/*= SD_EVENT_PRIORITY_NORMAL*/)
{
    auto const rv = ::sd_bus_attach_event(m_handle, e, priority);
    if (rv < 0) {
        throw error { "sd_bus_attach_event", rv };
    }
}

void bus::
attach_event(int const priority/* = SD_EVENT_PRIORITY_NORMAL*/)
{
    auto const rv = ::sd_bus_attach_event(m_handle, nullptr, priority);
    if (rv < 0) {
        throw error { "sd_bus_attach_event", rv };
    }
}

void bus::
detach_event()
{
    auto const rv = ::sd_bus_detach_event(m_handle);
    if (rv < 0) {
        throw error { "sd_bus_detach_event", rv };
    }
}

void bus::
enqueue_for_read(message& m)
{
    auto const rv = ::sd_bus_enqueue_for_read(m_handle, m);
    if (rv < 0) {
        throw error { "sd_bus_enqueue_for_read", rv };
    }
}

void bus::
start()
{
    auto const rv = ::sd_bus_start(m_handle);
    if (rv < 0) {
        throw error { "sd_bus_start", rv };
    }
}

void bus::
close() noexcept
{
    ::sd_bus_close(m_handle);
}

void bus::
flush()
{
    auto const rv = ::sd_bus_flush(m_handle);
    if (rv < 0) {
        throw error { "sd_bus_flush", rv };
    }
}

void bus::
wait(duration const timeout)
{
    auto const rv = ::sd_bus_wait(m_handle, timeout.count());
    if (rv < 0) {
        throw error { "sd_bus_wait", rv };
    }
}

std::optional<message> bus::
process()
{
    std::optional<message> result;
    ::sd_bus_message* m = nullptr;

    auto const rv = ::sd_bus_process(m_handle, &m);
    if (rv < 0) {
        throw error { "sd_bus_process", rv };
    }

    if (m) {
        return result.emplace(*m);
    }

    return result;
}

/*
 * free function
 */
bus
default_bus()
{
    ::sd_bus* result = nullptr;

    auto const rv = ::sd_bus_default(&result);
    if (rv < 0) {
        throw error { "sd_bus_default", rv };
    }

    assert(result);
    return *result;
}

bus
default_system_bus()
{
    ::sd_bus* result = nullptr;

    auto const rv = ::sd_bus_default_system(&result);
    if (rv < 0) {
        throw error { "sd_bus_default_system", rv };
    }

    assert(result);
    return *result;
}

bus
default_user_bus()
{
    ::sd_bus* result = nullptr;

    auto const rv = ::sd_bus_default_user(&result);
    if (rv < 0) {
        throw error { "sd_bus_default_user", rv };
    }

    assert(result);
    return *result;
}

bus
open_bus(cstring_view const description/*= nullptr*/)
{
    ::sd_bus* result = nullptr;
    auto const rv = ::sd_bus_open_with_description(&result, description);
    if (rv < 0) {
        throw error { "sd_bus_open_with_description", rv };
    }

    assert(result);
    return *result;
}

bus
open_user_bus(cstring_view const description/*= nullptr*/)
{
    ::sd_bus* result = nullptr;
    auto const rv = ::sd_bus_open_user_with_description(&result, description);
    if (rv < 0) {
        throw error { "sd_bus_open_user_with_description", rv };
    }

    assert(result);
    return *result;
}

bus
open_system_bus(cstring_view const description/*= nullptr*/)
{
    ::sd_bus* result = nullptr;
    auto const rv = ::sd_bus_open_system_with_description(&result, description);
    if (rv < 0) {
        throw error { "sd_bus_open_system_with_description", rv };
    }

    assert(result);
    return *result;
}

bus
open_user_machine(cstring_view const machine)
{
    ::sd_bus* result = nullptr;
    auto const rv = ::sd_bus_open_user_machine(&result, machine);
    if (rv < 0) {
        throw error { "sd_bus_open_user_machine", rv };
    }

    assert(result);
    return *result;
}

bus
open_system_machine(cstring_view const machine)
{
    ::sd_bus* result = nullptr;
    auto const rv = ::sd_bus_open_system_machine(&result, machine);
    if (rv < 0) {
        throw error { "sd_bus_open_system_machine", rv };
    }

    assert(result);
    return *result;
}

bus
open_system_remote(cstring_view const host)
{
    ::sd_bus* result = nullptr;
    auto const rv = ::sd_bus_open_system_remote(&result, host);
    if (rv < 0) {
        throw error { "sd_bus_open_system_remote", rv };
    }

    assert(result);
    return *result;
}

} // namespace stream9::sd_bus
