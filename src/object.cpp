#include <stream9/sd_bus/object.hpp>

#include <stream9/sd_bus/error.hpp>
#include <stream9/sd_bus/message.hpp>

#include <cassert>
#include <utility>

#include <stream9/json.hpp>
#include <stream9/strings.hpp>

namespace stream9::sd_bus {

object::
object(class bus b, cstring_view const path, cstring_view const interface,
       vtable&& vtbl)
    : m_vtbl { std::move(vtbl) }
{
    auto const userdata =
           const_cast<void*>(static_cast<void const*>(m_vtbl.data()));

    auto const rv = ::sd_bus_add_object_vtable(
                    b, &m_slot, path, interface, m_vtbl.table(), userdata);
    if (rv < 0) {
        throw error { "sd_bus_add_object_vtable", rv,
            str::to_string(json::object {
                { "path", path },
                { "interface", interface },
                { "vtbl", str::to_string(m_vtbl.table()) }, //TODO serialize vtable
                { "userdata", str::to_string(userdata) }
            }) };
    }

    assert(m_slot);
}

object::
~object() noexcept
{
    ::sd_bus_slot_unref(m_slot);
}

object::
object(object&& other) noexcept
    : m_slot { std::move(other.m_slot) }
    , m_vtbl { std::move(other.m_vtbl) }
{}

object& object::
operator=(object&& other) noexcept
{
    m_slot = std::move(other.m_slot);
    m_vtbl = std::move(other.m_vtbl);

    return *this;
}

class bus object::
bus() const noexcept
{
    auto const b = ::sd_bus_slot_get_bus(m_slot);
    assert(b);

    return *::sd_bus_ref(b);
}

std::optional<cstring_view> object::
description() const
{
    std::optional<cstring_view> result;

    char const* s = nullptr;
    auto const rc = ::sd_bus_slot_get_description(m_slot, &s);
    if (rc < 0) {
        if (rc == -ENXIO) {
            // nop
        }
        else {
            throw error { "sd_bus_slot_get_description", rc };
        }
    }
    else {
        result.emplace(s);
    }

    return result;
}

bool object::
is_floating() const
{
    auto const rc = ::sd_bus_slot_get_floating(m_slot);
    if (rc < 0) {
        throw error { "sd_bus_slot_get_floating", rc };
    }

    return rc;
}

::sd_bus_destroy_t object::
destroy_callback() const
{
    ::sd_bus_destroy_t result {};

    auto const rc = ::sd_bus_slot_get_destroy_callback(m_slot, &result);
    if (rc < 0) {
        throw error { "sd_bus_slot_get_destroy_callback", rc };
    }

    return result;
}

std::optional<message> object::
current_message() const
{
    std::optional<message> result;

    auto const m = ::sd_bus_slot_get_current_message(m_slot);
    if (m) {
        result.emplace(*::sd_bus_message_ref(m));
    }

    return result;
}

void object::
set_user_description(cstring_view const description)
{
    auto const rc = ::sd_bus_slot_set_description(m_slot, description);
    if (rc < 0) {
        throw error { "sd_bus_slot_set_description", rc };
    }
}

void object::
set_floating(bool const enable)
{
    auto const rc = ::sd_bus_slot_set_floating(m_slot, enable);
    if (rc < 0) {
        throw error { "sd_bus_slot_set_floating", rc };
    }
}

void object::
set_destroy_callback(::sd_bus_destroy_t const callback)
{
    auto const rc = ::sd_bus_slot_set_destroy_callback(m_slot, callback);
    if (rc < 0) {
        throw error { "sd_bus_slot_set_destroy_callback", rc };
    }
}

} // namespace stream9::sd_bus
