#include <stream9/sd_bus/message.hpp>

#include <stream9/sd_bus/bus.hpp>
#include <stream9/sd_bus/bus_creds.hpp>
#include <stream9/sd_bus/error.hpp>

#include <stream9/json.hpp>
#include <stream9/strings.hpp>

namespace stream9::sd_bus {

static cstring_view
message_type_to_symbol(uint8_t const type)
{
    switch (type) {
        case SD_BUS_MESSAGE_METHOD_CALL:
            return "SD_BUS_MESSAGE_METHOD_CALL";
        case SD_BUS_MESSAGE_METHOD_RETURN:
            return "SD_BUS_MESSAGE_METHOD_RETURN";
        case SD_BUS_MESSAGE_METHOD_ERROR:
            return "SD_BUS_MESSAGE_METHOD_ERROR";
        case SD_BUS_MESSAGE_SIGNAL:
            return "SD_BUS_MESSAGE_SIGNAL";
    }

    return "unknown message type";
}

static cstring_view
type_code_to_symbol(char const type)
{
    switch (type) {
        case SD_BUS_TYPE_BYTE:             return "SD_BUS_TYPE_BYTE";
        case SD_BUS_TYPE_BOOLEAN:          return "SD_BUS_TYPE_BOOLEAN";
        case SD_BUS_TYPE_INT16:            return "SD_BUS_TYPE_INT16";
        case SD_BUS_TYPE_UINT16:           return "SD_BUS_TYPE_UINT16";
        case SD_BUS_TYPE_INT32:            return "SD_BUS_TYPE_INT32";
        case SD_BUS_TYPE_UINT32:           return "SD_BUS_TYPE_UINT32";
        case SD_BUS_TYPE_INT64:            return "SD_BUS_TYPE_INT64";
        case SD_BUS_TYPE_UINT64:           return "SD_BUS_TYPE_UINT64";
        case SD_BUS_TYPE_DOUBLE:           return "SD_BUS_TYPE_DOUBLE";
        case SD_BUS_TYPE_STRING:           return "SD_BUS_TYPE_STRING";
        case SD_BUS_TYPE_OBJECT_PATH:      return "SD_BUS_TYPE_OBJECT_PATH";
        case SD_BUS_TYPE_SIGNATURE:        return "SD_BUS_TYPE_SIGNATURE";
        case SD_BUS_TYPE_UNIX_FD:          return "SD_BUS_TYPE_UNIX_FD";
        case SD_BUS_TYPE_ARRAY:            return "SD_BUS_TYPE_ARRAY";
        case SD_BUS_TYPE_VARIANT:          return "SD_BUS_TYPE_VARIANT";
        case SD_BUS_TYPE_STRUCT:           return "SD_BUS_TYPE_STRUCT";
        case SD_BUS_TYPE_STRUCT_BEGIN:     return "SD_BUS_TYPE_STRUCT_BEGIN";
        case SD_BUS_TYPE_STRUCT_END:       return "SD_BUS_TYPE_STRUCT_END";
        case SD_BUS_TYPE_DICT_ENTRY:       return "SD_BUS_TYPE_DICT_ENTRY";
        case SD_BUS_TYPE_DICT_ENTRY_BEGIN: return "SD_BUS_TYPE_DICT_ENTRY_BEGIN";
        case SD_BUS_TYPE_DICT_ENTRY_END:   return "SD_BUS_TYPE_DICT_ENTRY_END";
    }

    return "invalid type code";
}

/*
 * class message
 */
message::
message(class bus& b, uint8_t const type)
    : m_handle { nullptr }
{
    auto const rv = ::sd_bus_message_new(b, &m_handle, type);
    if (rv < 0) {
        throw error { "sd_bus_message_new", rv,
            str::to_string(json::object {
                { "type", message_type_to_symbol(type) }
            })
        };
    }
}

message::
message(::sd_bus_message& h) noexcept
    : m_handle { &h }
{}

message::
~message() noexcept
{
    if (m_handle) {
        ::sd_bus_message_unref(m_handle);
    }
}

message::
message(message const& other) noexcept
    : m_handle { ::sd_bus_message_ref(other) }
{}

message& message::
operator=(message const& other) noexcept
{
    message tmp { other };
    swap(tmp);

    return *this;
}

message::
message(message&& other) noexcept
    : m_handle { nullptr }
{
    swap(other);
}

message& message::
operator=(message&& other) noexcept
{
    swap(other);

    return *this;
}

void message::
swap(message& other) noexcept
{
    using std::swap;
    swap(m_handle, other.m_handle);
}

uint8_t message::
type() const
{
    uint8_t result {};

    auto const rv = ::sd_bus_message_get_type(m_handle, &result);
    if (rv < 0) {
        throw error { "sd_bus_message_get_type", rv };
    }

    return result;
}

bool message::
is_signal(cstring_view const interface/*= nullptr*/,
          cstring_view const member/*= nullptr*/) const
{
    auto const rv = ::sd_bus_message_is_signal(m_handle, interface, member);
    if (rv < 0) {
        throw error { "sd_bus_message_is_signal", rv,
            str::to_string(json::object {
                { "interface", interface },
                { "member", member },
            })
        };
    }

    return rv;
}

bool message::
is_method_call(cstring_view const interface/*= nullptr*/,
               cstring_view const member/*= nullptr*/) const
{
    auto const rv = ::sd_bus_message_is_method_call(m_handle, interface, member);
    if (rv < 0) {
        throw error { "sd_bus_message_is_method_call", rv,
            str::to_string(json::object {
                { "interface", interface },
                { "member", member },
            })
        };
    }

    return rv;
}

bool message::
is_method_error(cstring_view const name/*= nullptr*/) const
{
    auto const rv = ::sd_bus_message_is_method_error(m_handle, name);
    if (rv < 0) {
        throw error { "sd_bus_message_is_method_error", rv,
            str::to_string(json::object {
                { "name", name },
            })
        };
    }

    return rv;
}

bool message::
is_empty() const noexcept
{
    return ::sd_bus_message_is_empty(m_handle);
}

bool message::
expect_reply() const noexcept
{
    return ::sd_bus_message_get_expect_reply(m_handle);
}

bool message::
auto_start() const noexcept
{
    return ::sd_bus_message_get_auto_start(m_handle);
}

bool message::
allow_interactive_authorization() const noexcept
{
    return ::sd_bus_message_get_allow_interactive_authorization(m_handle);
}

bool message::
has_signature(cstring_view const signature) const noexcept
{
    return ::sd_bus_message_has_signature(m_handle, signature);
}

cstring_view message::
signature(bool const complete) const noexcept
{
    return ::sd_bus_message_get_signature(m_handle, complete);
}

cstring_view message::
path() const noexcept
{
    return ::sd_bus_message_get_path(m_handle);
}

cstring_view message::
interface() const noexcept
{
    return ::sd_bus_message_get_interface(m_handle);
}

cstring_view message::
member() const noexcept
{
    return ::sd_bus_message_get_member(m_handle);
}

cstring_view message::
destination() const noexcept
{
    return ::sd_bus_message_get_destination(m_handle);
}

cstring_view message::
sender() const noexcept
{
    return ::sd_bus_message_get_destination(m_handle);
}

class bus message::
bus() const noexcept
{
    return *::sd_bus_ref(
        ::sd_bus_message_get_bus(m_handle) );
}

bus_creds message::
creds() const noexcept
{
    return *::sd_bus_creds_ref(
        ::sd_bus_message_get_creds(m_handle) );
}

std::optional<uint64_t> message::
cookie() const
{
    std::optional<uint64_t> result;
    uint64_t cookie {};

    auto const rv = ::sd_bus_message_get_cookie(m_handle, &cookie);
    if (rv >= 0) {
        result.emplace(cookie);
    }
    else if (rv == -ENODATA) {
        // nop
    }
    else {
        throw error { "sd_bus_message_get_cookie", rv };
    }

    return result;
}

std::optional<uint64_t> message::
reply_cookie() const
{
    std::optional<uint64_t> result;
    uint64_t cookie {};

    auto const rv = ::sd_bus_message_get_reply_cookie(m_handle, &cookie);
    if (rv >= 0) {
        result.emplace(cookie);
    }
    else if (rv == -ENODATA) {
        // nop
    }
    else {
        throw error { "sd_bus_message_get_reply_cookie", rv };
    }

    return result;
}

std::optional<duration> message::
monotonic_usec() const
{
    std::optional<duration> result;
    uint64_t usec {};

    auto const rv = ::sd_bus_message_get_monotonic_usec(m_handle, &usec);
    if (rv >= 0) {
        result.emplace(usec);
    }
    else if (rv == -ENODATA) {
        // nop
    }
    else {
        throw error { "sd_bus_message_get_monotonic_usec", rv };
    }

    return result;
}

std::optional<duration> message::
realtime_usec() const
{
    std::optional<duration> result;
    uint64_t usec {};

    auto const rv = ::sd_bus_message_get_realtime_usec(m_handle, &usec);
    if (rv >= 0) {
        result.emplace(usec);
    }
    else if (rv == -ENODATA) {
        // nop
    }
    else {
        throw error { "sd_bus_message_get_realtime_usec", rv };
    }

    return result;
}

std::optional<uint64_t> message::
seqnum() const
{
    std::optional<uint64_t> result;
    uint64_t num {};

    auto const rv = ::sd_bus_message_get_seqnum(m_handle, &num);
    if (rv >= 0) {
        result.emplace(num);
    }
    else if (rv == -ENODATA) {
        // nop
    }
    else {
        throw error { "sd_bus_message_get_seqnum", rv };
    }

    return result;
}

void message::
read(cstring_view const type, ...)
{
    va_list ap;

    va_start(ap, type);
    auto const rv = ::sd_bus_message_readv(m_handle, type, ap);
    va_end(ap);

    if (rv < 0) {
        throw error { "sd_bus_message_readv", rv,
            str::to_string(json::object {
                { "type", type },
            })
        };
    }
}

void message::
read_basic(char const type, void* const p)
{
    auto const rv = ::sd_bus_message_read_basic(m_handle, type, p);
    if (rv < 0) {
        throw error { "sd_bus_message_read_basic", rv,
            str::to_string(json::object {
                { "type", type_code_to_symbol(type) },
                { "p", str::to_string(p) },
            })
        };
    }
}

void message::
read_array(char const type, void const** const ptr, size_t* const size)
{
    auto const rv = ::sd_bus_message_read_array(m_handle, type, ptr, size);
    if (rv < 0) {
        throw error { "sd_bus_message_read_array", rv,
            str::to_string(json::object {
                { "type", type_code_to_symbol(type) },
                { "ptr", str::to_string(ptr) },
                { "size", str::to_string(size) },
            })
        };
    }
}

std::unique_ptr<char**> message::
read_strv()
{
    char** result = nullptr;
    auto const rv = ::sd_bus_message_read_strv(m_handle, &result);
    if (rv < 0) {
        throw error { "sd_bus_message_read_strv", rv };
    }

    return std::make_unique<char**>(result);
}

void message::
enter_container(char const type, cstring_view const contents)
{
    auto const rv = ::sd_bus_message_enter_container(m_handle, type, contents);
    if (rv < 0) {
        throw error { "sd_bus_message_enter_container", rv,
            str::to_string(json::object {
                { "type", type_code_to_symbol(type) },
                { "contents", contents },
            })
        };
    }
}

void message::
exit_container()
{
    auto const rv = ::sd_bus_message_exit_container(m_handle);
    if (rv < 0) {
        throw error { "sd_bus_message_exit_container", rv };
    }
}

bool message::
peek_type(char* const type, char const** const contents)
{
    auto const rv = ::sd_bus_message_peek_type(m_handle, type, contents);
    if (rv < 0) {
        throw error { "sd_bus_message_peek_type", rv };
    }

    return rv;
}

std::optional<message::peek_type_result> message::
peek_type()
{
    char type {};
    char const* contents {};
    std::optional<peek_type_result> result;

    auto const rc = ::sd_bus_message_peek_type(m_handle, &type, &contents);
    if (rc < 0) {
        throw error { "sd_bus_message_peek_type", rc };
    }

    if (rc != 0) {
        result.emplace(type, contents);
    }

    return result;
}

bool message::
verify_type(char const type, cstring_view const contents)
{
    auto const rc = ::sd_bus_message_verify_type(m_handle, type, contents);
    if (rc < 0) {
        throw error { "sd_bus_message_verify_type", rc,
            str::to_string(json::object {
                { "type", type_code_to_symbol(type) },
                { "contents", contents },
            })
        };
    }

    return rc != 0;
}

bool message::
at_end(bool const complete/*= false*/)
{
    auto const rv = ::sd_bus_message_at_end(m_handle, complete);
    if (rv < 0) {
        throw error { "sd_bus_message_at_end", rv,
            str::to_string(json::object {
                { "complete", complete },
            })
        };
    }

    return rv;
}

std::optional<class dbus_error> message::
dbus_error() const noexcept
{
    std::optional<class dbus_error> result;

    auto* const e =  ::sd_bus_message_get_error(m_handle);
    if (e) {
        result.emplace(*e);
    }

    return result;
}

int message::
get_errno() const noexcept
{
    return ::sd_bus_message_get_errno(m_handle);
}

void message::
set_expect_reply(bool const enable)
{
    auto const rv = ::sd_bus_message_set_expect_reply(m_handle, enable);
    if (rv < 0) {
        throw error { "sd_bus_message_set_expect_reply", rv,
            str::to_string(json::object {
                { "enable", enable },
            })
        };
    }
}

void message::
set_auto_start(bool const enable)
{
    auto const rv = ::sd_bus_message_set_auto_start(m_handle, enable);
    if (rv < 0) {
        throw error { "sd_bus_message_set_auto_start", rv,
            str::to_string(json::object {
                { "enable", enable },
            })
        };
    }
}

void message::
set_allow_interactive_authorization(bool const enable)
{
    auto const rv =
        ::sd_bus_message_set_allow_interactive_authorization(m_handle, enable);
    if (rv < 0) {
        throw error { "sd_bus_message_set_allow_interactive_authorization", rv,
            str::to_string(json::object {
                { "enable", enable },
            })
        };
    }
}

void message::
set_destination(cstring_view const destination)
{
    auto const rv = ::sd_bus_message_set_destination(m_handle, destination);
    if (rv < 0) {
        throw error { "sd_bus_message_set_destination", rv,
            str::to_string(json::object {
                { "destination", destination },
            })
        };
    }
}

void message::
set_sender(cstring_view const sender)
{
    auto const rv = ::sd_bus_message_set_sender(m_handle, sender);
    if (rv < 0) {
        throw error { "sd_bus_message_set_sender", rv,
            str::to_string(json::object {
                { "sender", sender },
            })
        };
    }
}

void message::
mark_sensitive()
{
    auto const rc = ::sd_bus_message_sensitive(m_handle);
    if (rc < 0) {
        throw error { "sd_bus_message_sensitive", rc };
    }
}

void message::
append(cstring_view const type, ...)
{
    va_list ap;

    va_start(ap, type);
    auto const rv = ::sd_bus_message_appendv(m_handle, type, ap);
    va_end(ap);

    if (rv < 0) {
        throw error { "sd_bus_message_appendv", rv,
            str::to_string(json::object {
                { "type", type },
            })
        };
    }
}

void message::
append_basic(char const type, void const* const p)
{
    auto const rv = ::sd_bus_message_append_basic(m_handle, type, p);
    if (rv < 0) {
        throw error { "sd_bus_message_append_basic", rv,
            str::to_string(json::object {
                { "type", type_code_to_symbol(type) },
                { "p", str::to_string(p) },
            })
        };
    }
}

void message::
append_array(char const type, void const* const ptr, size_t const size)
{
    auto const rv = ::sd_bus_message_append_array(m_handle, type, ptr, size);
    if (rv < 0) {
        throw error { "sd_bus_message_append_array", rv,
            str::to_string(json::object {
                { "type", type_code_to_symbol(type) },
                { "ptr", str::to_string(ptr) },
                { "size", size },
            })
        };
    }
}

void message::
append_array_space(char const type, size_t const size, void** const ptr)
{
    auto const rv =
        ::sd_bus_message_append_array_space(m_handle, type, size, ptr);
    if (rv < 0) {
        throw error { "sd_bus_message_append_array_space", rv,
            str::to_string(json::object {
                { "type", type_code_to_symbol(type) },
                { "size", size },
                { "ptr", str::to_string(ptr) },
            })
        };
    }
}

void message::
append_array_iovec(char const type,
                   struct ::iovec const* const iov, unsigned const n)
{
    auto const rv = ::sd_bus_message_append_array_iovec(m_handle, type, iov, n);
    if (rv < 0) {
        throw error { "sd_bus_message_append_array_iovec", rv,
            str::to_string(json::object {
                { "type", type_code_to_symbol(type) },
                { "iov", str::to_string(iov) },
                { "n", n },
            })
        };
    }
}

void message::
append_array_memfd(char const type, int memfd,
                   uint64_t const offset, uint64_t const size)
{
    auto const rv =
        ::sd_bus_message_append_array_memfd(m_handle, type, memfd, offset, size);
    if (rv < 0) {
        throw error { "sd_bus_message_append_array_memfd", rv,
            str::to_string(json::object {
                { "type", type_code_to_symbol(type) },
                { "memfd", memfd },
                { "offset", offset },
                { "size", size },
            })
        };
    }
}

void message::
append_string_space(size_t const size, char** const s)
{
    auto const rv =
        ::sd_bus_message_append_string_space(m_handle, size, s);
    if (rv < 0) {
        throw error { "sd_bus_message_append_string_space", rv,
            str::to_string(json::object {
                { "size", size },
                { "s", str::to_string(s) },
            })
        };
    }
}

void message::
append_string_iovec(struct ::iovec const* const iov, unsigned const n)
{
    auto const rv =
        ::sd_bus_message_append_string_iovec(m_handle, iov, n);
    if (rv < 0) {
        throw error { "sd_bus_message_append_string_iovec", rv,
            str::to_string(json::object {
                { "iov", str::to_string(iov) },
                { "n", n },
            })
        };
    }
}

void message::
append_string_memfd(int const memfd, uint64_t const offset, uint64_t const size)
{
    auto const rv =
        ::sd_bus_message_append_string_memfd(m_handle, memfd, offset, size);
    if (rv < 0) {
        throw error { "sd_bus_message_append_string_memfd", rv,
            str::to_string(json::object {
                { "memfd", memfd },
                { "offset", offset },
                { "size", size },
            })
        };
    }
}

void message::
append_strv(char** const l)
{
    auto const rv = ::sd_bus_message_append_strv(m_handle, l);
    if (rv < 0) {
        throw error { "sd_bus_message_append_strv", rv,
            str::to_string(json::object {
                { "l", str::to_string(l) },
            })
        };
    }
}

void message::
open_container(char const type, cstring_view const contents)
{
    auto const rv = ::sd_bus_message_open_container(m_handle, type, contents);
    if (rv < 0) {
        throw error { "sd_bus_message_open_container", rv,
            str::to_string(json::object {
                { "type", type_code_to_symbol(type) },
                { "contents", contents },
            })
        };
    }
}

void message::
close_container()
{
    auto const rv = ::sd_bus_message_close_container(m_handle);
    if (rv < 0) {
        throw error { "sd_bus_message_close_container", rv };
    }
}

void message::
copy(message const& source, bool const all)
{
    auto const rv = ::sd_bus_message_copy(m_handle, source.m_handle, all);
    if (rv < 0) {
        throw error { "sd_bus_message_copy", rv,
            str::to_string(json::object {
                { "all", all },
            })
        };
    }
}

void message::
seal(uint64_t const cookie, duration const timeout/*= {}*/)
{
    auto const rv = ::sd_bus_message_seal(m_handle, cookie, timeout.count());
    if (rv < 0) {
        throw error { "sd_bus_message_seal", rv,
            str::to_string(json::object {
                { "cookie", cookie },
                { "timeout", str::to_string(timeout) },
            })
        };
    }
}

void message::
skip(cstring_view const type)
{
    auto const rv = ::sd_bus_message_skip(m_handle, type);
    if (rv < 0) {
        throw error { "sd_bus_message_skip", rv,
            str::to_string(json::object {
                { "type", type },
            })
        };
    }
}

void message::
rewind(bool const complete/*= false*/)
{
    auto const rv = ::sd_bus_message_rewind(m_handle, complete);
    if (rv < 0) {
        throw error { "sd_bus_message_rewind", rv,
            str::to_string(json::object {
                { "complete", complete },
            })
        };
    }
}

void message::
dump(FILE* const f, uint64_t const flags) const
{
    auto const rv = ::sd_bus_message_dump(m_handle, f, flags);
    if (rv < 0) {
        throw error { "sd_bus_message_dump", rv,
            str::to_string(json::object {
                { "flags", flags },
            })
        };
    }
}

/*
 * class signal
 */
static ::sd_bus_message&
new_signal(bus& b,
           cstring_view const path,
           cstring_view const interface,
           cstring_view const member)
{
    ::sd_bus_message* result = nullptr;

    auto const rv =
        ::sd_bus_message_new_signal(b, &result, path, interface, member);
    if (rv < 0) {
        throw error { "sd_bus_message_new_signal", rv,
            str::to_string(json::object {
                { "path", path },
                { "interface", interface },
                { "member", member },
            })
        };
    }

    assert(result);
    return *result;
}

signal::
signal(class bus& b,
       cstring_view const path,
       cstring_view const interface,
       cstring_view const member)
    : message { new_signal(b, path, interface, member) }
{}

signal::
signal(::sd_bus_message& m) noexcept
    : message { m }
{
    assert(this->is_signal());
}

/*
 * class method_call
 */
static ::sd_bus_message&
new_method_call(bus& b,
                cstring_view const destination,
                cstring_view const path,
                cstring_view const interface,
                cstring_view const member)
{
    ::sd_bus_message* result = nullptr;

    auto const rv = ::sd_bus_message_new_method_call(
                           b, &result, destination, path, interface, member );
    if (rv < 0) {
        throw error { "sd_bus_message_new_method_call", rv,
            str::to_string(json::object {
                { "destination", destination },
                { "path", path },
                { "interface", interface },
                { "member", member },
            })
        };
    }

    assert(result);
    return *result;
}

method_call::
method_call(class bus& b,
            cstring_view const destination,
            cstring_view const path,
            cstring_view const interface,
            cstring_view const member)
    : message { new_method_call(b, destination, path, interface, member) }
{}

method_call::
method_call(::sd_bus_message& m) noexcept
    : message { m }
{
    assert(this->is_method_call());
}

/*
 * class method_return
 */
static ::sd_bus_message&
new_method_return(message const& m)
{
    ::sd_bus_message* result = nullptr;

    auto const rv = ::sd_bus_message_new_method_return(m, &result);
    if (rv < 0) {
        throw error { "sd_bus_message_new_method_return", rv };
    }

    assert(result);
    return *result;
}

method_return::
method_return(message const& m)
    : message { new_method_return(m) }
{}

method_return::
method_return(::sd_bus_message& m) noexcept
    : message { m }
{
    assert(this->type() == SD_BUS_MESSAGE_METHOD_RETURN);
}

/*
 * method_error
 */
static ::sd_bus_message&
new_method_error(method_call const& m, ::sd_bus_error const& e)
{
    ::sd_bus_message* result = nullptr;

    auto const rv = ::sd_bus_message_new_method_error(m, &result, &e);
    if (rv < 0) {
        throw error { "sd_bus_message_new_method_error", rv };
    }

    assert(result);
    return *result;
}

static ::sd_bus_message&
new_method_error(method_call const& m, int const error, ::sd_bus_error const* e)
{
    ::sd_bus_message* result = nullptr;

    auto const rv =
        ::sd_bus_message_new_method_errno(m, &result, error, e);
    if (rv < 0) {
        throw sd_bus::error { "sd_bus_message_new_method_errno", rv,
            str::to_string(json::object {
                { "error", error },
            })
        };
    }

    assert(result);
    return *result;
}

method_error::
method_error(method_call const& m, ::sd_bus_error const& e)
    : message { new_method_error(m, e) }
{}

method_error::
method_error(method_call const& m,
             int const error, ::sd_bus_error const* e/*= nullptr*/)
    : message { new_method_error(m, error, e) }
{}

method_error::
method_error(::sd_bus_message& m) noexcept
    : message { m }
{
    assert(this->type() == SD_BUS_MESSAGE_METHOD_ERROR);
}

#if 0
method_error
make_method_error(method_call const& m,
             cstring_view name, cstring_view format, ...)
{
    ::sd_bus_message* result = nullptr;

    //_cleanup_(sd_bus_error_free) sd_bus_error error = SD_BUS_ERROR_NULL;
    ::sd_bus_error error = SD_BUS_ERROR_NULL;
    va_list ap;

    if (!name) throw sd_bus::error { -EINVAL };

    va_start(ap, format);
    ::bus_error_setfv(&error, name, format, ap);
    va_end(ap);

    auto const rv =
        ::sd_bus_message_new_method_error(m, &result, &error);
    if (rv < 0) {
        throw error { "sd_bus_message_new_method_error", rv };
    }

    return result;
}
#endif

void
reply_method_return(method_call const& m, cstring_view types, ...)
{
    va_list ap;

    va_start(ap, types);
    auto const rv = ::sd_bus_reply_method_returnv(m, types, ap);
    va_end(ap);

    if (rv < 0) {
        throw error { "sd_bus_reply_method_returnv", rv,
            str::to_string(json::object {
                { "types", types },
            })
        };
    }
}

void
reply_method_error(method_call const& m, dbus_error const& e)
{
    auto const rv = ::sd_bus_reply_method_error(m, &e.base());
    if (rv < 0) {
        throw error { "sd_bus_reply_method_error", rv };
    }
}

void
reply_method_error(method_call const& m,
                   cstring_view const name, cstring_view const format, ...)
{
    va_list ap;

    va_start(ap, format);
    auto const rv = ::sd_bus_reply_method_errorfv(m, name, format, ap);
    va_end(ap);

    if (rv < 0) {
        throw error { "sd_bus_reply_method_errorfv", rv,
            str::to_string(json::object {
                { "name", name },
                { "format", format },
            })
        };
    }
}

void
reply_method_errno(method_call const& m, int const error, dbus_error const& e)
{
    auto const rv = ::sd_bus_reply_method_errno(m, error, &e.base());
    if (rv < 0) {
        throw sd_bus::error { "sd_bus_reply_method_errno", rv,
            str::to_string(json::object {
                { "error", error },
            })
        };
    }
}

void
reply_method_errno(method_call const& m,
                   int const error, cstring_view const format, ...)
{
    va_list ap;

    va_start(ap, format);
    auto const rv = ::sd_bus_reply_method_errnofv(m, error, format, ap);
    va_end(ap);

    if (rv < 0) {
        throw sd_bus::error { "sd_bus_reply_method_errnofv", rv,
            str::to_string(json::object {
                { "error", error },
                { "format", format },
            })
        };
    }
}

} // namespace stream9::sd_bus
