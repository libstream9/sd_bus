#include <stream9/sd_bus/bus_creds.hpp>

#include <stream9/sd_bus/bus.hpp>
#include <stream9/sd_bus/error.hpp>

#include <cassert>

namespace stream9::sd_bus {

template<typename T>
static std::optional<T>
get_id(auto const handle, auto func, std::string_view const what)
{
    std::optional<T> result;
    T id {};

    auto const rv = func(handle, &id);
    if (rv >= 0) {
        result.emplace(id);
    }
    else if (rv == -ENODATA) {
        // nop
    }
    else {
        throw error { std::string(what), rv };
    }

    return result;
}

static std::optional<cstring_view>
get_cstring_view(auto const handle, auto func, std::string_view const what)
{
    std::optional<cstring_view> result;
    char const* s {};

    auto const rv = func(handle, &s);
    if (rv >= 0) {
        result.emplace(s);
    }
    else if (rv == -ENODATA) {
        // nop
    }
    else {
        throw error { std::string(what), rv };
    }

    return result;
}

static std::optional<bool>
get_cap(auto const handle, auto func, int capability, std::string_view const what)
{
    std::optional<bool> result;

    auto const rv = func(handle, capability);
    if (rv >= 0) {
        result.emplace(rv);
    }
    else if (rv == -ENODATA) {
        // nop
    }
    else {
        throw error { std::string(what), rv };
    }

    return result;
}

bus_creds::
bus_creds(pid_t const pid, uint64_t const creds_mask)
    : m_handle { nullptr }
{
    auto const rv = ::sd_bus_creds_new_from_pid(&m_handle, pid, creds_mask);
    if (rv < 0) {
        throw error { "sd_bus_creds_new_from_pid", rv };
    }

    assert(m_handle);
}

bus_creds::
bus_creds(::sd_bus_creds& creds) noexcept
    : m_handle { &creds }
{}

bus_creds::
~bus_creds() noexcept
{
    ::sd_bus_creds_unref(m_handle);
}

bus_creds::
bus_creds(bus_creds const& other) noexcept
    : m_handle { ::sd_bus_creds_ref(other.m_handle) }
{}

bus_creds& bus_creds::
operator=(bus_creds const& other) noexcept
{
    ::sd_bus_creds_unref(m_handle);
    m_handle = ::sd_bus_creds_ref(other.m_handle);

    return *this;
}

uint64_t bus_creds::
mask() const noexcept
{
    return ::sd_bus_creds_get_mask(m_handle);
}

uint64_t bus_creds::
augmented_mask() const noexcept
{
    return ::sd_bus_creds_get_augmented_mask(m_handle);
}

std::optional<pid_t> bus_creds::
pid() const
{
    return get_id<pid_t>(m_handle, ::sd_bus_creds_get_pid, "sd_bus_creds_get_pid");
}

std::optional<pid_t> bus_creds::
ppid() const
{
    return get_id<pid_t>(m_handle, ::sd_bus_creds_get_ppid, "sd_bus_creds_get_ppid");
}

std::optional<pid_t> bus_creds::
tid() const
{
    return get_id<pid_t>(m_handle, ::sd_bus_creds_get_tid, "sd_bus_creds_get_tid");
}

std::optional<uid_t> bus_creds::
uid() const
{
    return get_id<uid_t>(m_handle, ::sd_bus_creds_get_uid, "sd_bus_creds_get_uid");
}

std::optional<uid_t> bus_creds::
euid() const
{
    return get_id<uid_t>(m_handle, ::sd_bus_creds_get_euid, "sd_bus_creds_get_euid");
}

std::optional<uid_t> bus_creds::
suid() const
{
    return get_id<uid_t>(m_handle, ::sd_bus_creds_get_suid, "sd_bus_creds_get_suid");
}

std::optional<uid_t> bus_creds::
fsuid() const
{
    return get_id<uid_t>(m_handle, ::sd_bus_creds_get_fsuid, "sd_bus_creds_get_fsuid");
}

std::optional<uid_t> bus_creds::
owner_uid() const
{
    return get_id<uid_t>(m_handle, ::sd_bus_creds_get_owner_uid, "sd_bus_creds_get_owner_uid");
}

std::optional<gid_t> bus_creds::
gid() const
{
    return get_id<gid_t>(m_handle, ::sd_bus_creds_get_gid, "sd_bus_creds_get_gid");
}

std::optional<gid_t> bus_creds::
egid() const
{
    return get_id<gid_t>(m_handle, ::sd_bus_creds_get_egid, "sd_bus_creds_get_egid");
}

std::optional<gid_t> bus_creds::
sgid() const
{
    return get_id<gid_t>(m_handle, ::sd_bus_creds_get_sgid, "sd_bus_creds_get_sgid");
}

std::optional<gid_t> bus_creds::
fsgid() const
{
    return get_id<gid_t>(m_handle, ::sd_bus_creds_get_fsgid, "sd_bus_creds_get_fsgid");
}

std::span<gid_t const> bus_creds::
supplementary_gids() const
{
    gid_t const* result = nullptr;

    auto const rv = ::sd_bus_creds_get_supplementary_gids(m_handle, &result);
    if (rv >= 0) {
        return { result, static_cast<size_t>(rv) };
    }
    else if (rv == -ENODATA) {
        return {};
    }
    else {
        throw error { "sd_bus_creds_get_supplementary_gids", rv };
    }
}

std::optional<cstring_view> bus_creds::
comm() const
{
    return get_cstring_view(m_handle, ::sd_bus_creds_get_comm, "sd_bus_creds_get_comm");
}

std::optional<cstring_view> bus_creds::
tid_comm() const
{
    return get_cstring_view(m_handle, ::sd_bus_creds_get_tid_comm, "sd_bus_creds_get_tid_comm");
}

std::optional<cstring_view> bus_creds::
exe() const
{
    return get_cstring_view(m_handle, ::sd_bus_creds_get_exe, "sd_bus_creds_get_exe");
}

cstring_view* bus_creds::
cmdline() const
{
    char** result = nullptr;

    auto const rv = ::sd_bus_creds_get_cmdline(m_handle, &result);
    if (rv < 0) {
        throw error { "sd_bus_creds_get_cmdline", rv };
    }

    return reinterpret_cast<cstring_view*>(result);
}

std::optional<cstring_view> bus_creds::
cgroup() const
{
    return get_cstring_view(m_handle, ::sd_bus_creds_get_cgroup, "sd_bus_creds_get_cgroup");
}

std::optional<cstring_view> bus_creds::
unit() const
{
    return get_cstring_view(m_handle, ::sd_bus_creds_get_unit, "sd_bus_creds_get_unit");
}

std::optional<cstring_view> bus_creds::
slice() const
{
    return get_cstring_view(m_handle, ::sd_bus_creds_get_slice, "sd_bus_creds_get_slice");
}

std::optional<cstring_view> bus_creds::
user_unit() const
{
    return get_cstring_view(m_handle, ::sd_bus_creds_get_user_unit, "sd_bus_creds_get_user_unit");
}

std::optional<cstring_view> bus_creds::
user_slice() const
{
    return get_cstring_view(m_handle, ::sd_bus_creds_get_user_slice, "sd_bus_creds_get_user_slice");
}

std::optional<cstring_view> bus_creds::
session() const
{
    return get_cstring_view(m_handle, ::sd_bus_creds_get_session, "sd_bus_creds_get_session");
}

std::optional<cstring_view> bus_creds::
selinux_context() const
{
    return get_cstring_view(m_handle, ::sd_bus_creds_get_selinux_context, "sd_bus_creds_get_selinux_context");
}

std::optional<cstring_view> bus_creds::
tty() const
{
    return get_cstring_view(m_handle, ::sd_bus_creds_get_tty, "sd_bus_creds_get_tty");
}

std::optional<cstring_view> bus_creds::
description() const
{
    return get_cstring_view(m_handle, ::sd_bus_creds_get_description, "sd_bus_creds_get_description");
}

std::optional<cstring_view> bus_creds::
unique_name() const
{
    return get_cstring_view(m_handle, ::sd_bus_creds_get_unique_name, "sd_bus_creds_get_unique_name");
}

cstring_view* bus_creds::
well_known_names() const
{
    char** result = nullptr;

    auto const rv = ::sd_bus_creds_get_well_known_names(m_handle, &result);
    if (rv < 0) {
        throw error { "sd_bus_creds_get_well_known_names", rv };
    }

    assert(result);
    return reinterpret_cast<cstring_view*>(result);
}

std::optional<bool> bus_creds::
has_effective_cap(int const capability) const
{
    return get_cap(m_handle, ::sd_bus_creds_has_effective_cap, capability, "sd_bus_creds_has_effective_cap");
}

std::optional<bool> bus_creds::
has_permitted_cap(int const capability) const
{
    return get_cap(m_handle, ::sd_bus_creds_has_permitted_cap, capability, "sd_bus_creds_has_permitted_cap");
}

std::optional<bool> bus_creds::
has_inheritable_cap(int const capability) const
{
    return get_cap(m_handle, ::sd_bus_creds_has_inheritable_cap, capability, "sd_bus_creds_has_inheritable_cap");
}

std::optional<bool> bus_creds::
has_bounding_cap(int const capability) const
{
    return get_cap(m_handle, ::sd_bus_creds_has_bounding_cap, capability, "sd_bus_creds_has_bounding_cap");
}

std::optional<uint32_t> bus_creds::
audit_session_id() const
{
    return get_id<uint32_t>(m_handle, ::sd_bus_creds_get_audit_session_id, "sd_bus_creds_get_audit_session_id");
}

std::optional<uid_t> bus_creds::
audit_login_uid() const
{
    return get_id<uid_t>(m_handle, ::sd_bus_creds_get_audit_login_uid, "sd_bus_creds_get_audit_login_uid");
}

bus_creds
name_creds(bus& b, str::cstring_view const name, uint64_t const mask)
{
    ::sd_bus_creds* result = nullptr;
    auto const rv = ::sd_bus_get_name_creds(b, name, mask, &result);
    if (rv < 0) {
        throw error { "sd_bus_get_name_creds", rv };
    }

    assert(result);
    return *result;
}

} // namespace stream9::sd_bus
