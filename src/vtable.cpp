#include <stream9/sd_bus/vtable.hpp>

#include <stream9/sd_bus/bus.hpp>
#include <stream9/sd_bus/error.hpp>
#include <stream9/sd_bus/message.hpp>

namespace stream9::sd_bus {

#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wmissing-field-initializers" // for SD_BUS_VTABLE_END

struct method_data {
    message_handler handler {};
};

struct property_data {
    property_handler get {};
    property_handler set {};
};

union vtable_data
{
    vtable_data(method_data d) : method { std::move(d) } {}
    vtable_data(property_data d) : property { std::move(d) } {}

    ~vtable_data() {}

    method_data method;
    property_data property;
};

static int
on_method(::sd_bus_message* const m,
           void* const userdata,
           ::sd_bus_error* const e)
{
    assert(m);
    assert(userdata);
    assert(e);

    auto* const data = static_cast<vtable_data*>(userdata);

    return data->method.handler(*::sd_bus_message_ref(m), *e);
}

static int
on_get(::sd_bus* const bus,
       char const* const path,
       char const* const interface,
       char const* const property,
       ::sd_bus_message* const m,
       void* const userdata,
       ::sd_bus_error* const e)
{
    assert(bus);
    assert(path);
    (void)interface;
    assert(property);
    assert(m);
    assert(userdata);
    assert(e);

    auto* const data = static_cast<vtable_data*>(userdata);

    return data->property.get(
        *::sd_bus_ref(bus),
        path,
        interface,
        property,
        *::sd_bus_message_ref(m),
        *e );
}

static int
on_set(::sd_bus* const bus,
       char const* const path,
       char const* const interface,
       char const* const property,
       ::sd_bus_message* const m,
       void* const userdata,
       ::sd_bus_error* const e)
{
    assert(bus);
    assert(path);
    (void)interface;
    assert(property);
    assert(m);
    assert(userdata);
    assert(e);

    auto* const data = static_cast<vtable_data*>(userdata);

    return data->property.set(
        *::sd_bus_ref(bus),
        path,
        interface,
        property,
        *::sd_bus_message_ref(m),
        *e );
}

vtable::
vtable(uint64_t flags/*= 0*/)
{
    m_vtbl.push_back(SD_BUS_VTABLE_START(flags));
    m_vtbl.push_back(SD_BUS_VTABLE_END);
}

vtable::~vtable() = default;

vtable::vtable(vtable&&) noexcept = default;
vtable& vtable::operator=(vtable&&) noexcept = default;

void vtable::
add_method(cstring_view const member,
           cstring_view const args,
           cstring_view const result,
           message_handler h,
           uint64_t const flags)
{
    auto const offset = m_data.size();

    method_data data;
    data.handler = std::move(h);
    m_data.push_back(std::make_unique<vtable_data>(std::move(data)));

    m_vtbl.insert(m_vtbl.end() - 1,
        SD_BUS_METHOD_WITH_ARGS_OFFSET(
            member, args, result, on_method, offset, flags ) );
}

void vtable::
add_signal(cstring_view const member,
           cstring_view const signature,
           uint64_t const flags)
{
    m_vtbl.insert(m_vtbl.end() - 1,
        SD_BUS_SIGNAL_WITH_ARGS(member, signature, flags) );
}

void vtable::
add_property(cstring_view const member,
             cstring_view const signature,
             property_handler get,
             uint64_t const flags)
{
    auto const offset = m_data.size();

    property_data data;
    data.get = std::move(get);
    m_data.push_back(std::make_unique<vtable_data>(std::move(data)));

    m_vtbl.insert(m_vtbl.end() - 1,
        SD_BUS_PROPERTY(
            member, signature, on_get, offset, flags ) );
}

void vtable::
add_property(cstring_view const member,
             cstring_view const signature,
             property_handler get,
             property_handler set,
             uint64_t const flags)
{
    auto const offset = m_data.size();

    property_data data;
    data.get = std::move(get);
    data.set = std::move(set);
    m_data.push_back(std::make_unique<vtable_data>(std::move(data)));

    m_vtbl.insert(m_vtbl.end() - 1,
        SD_BUS_WRITABLE_PROPERTY(
            member, signature, on_get, on_set, offset, flags ) ); //XXX
}

} // namespace stream9::sd_bus
