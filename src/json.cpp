#include <stream9/sd_bus/json.hpp>

#include "namespace.hpp"

#include <stream9/sd_bus/duration.hpp>
#include <stream9/sd_bus/message.hpp>
#include <stream9/sd_bus/slot.hpp>

#include <systemd/sd-bus.h>

namespace stream9::sd_bus {

static std::string_view
type_to_symbol(uint8_t const type)
{
    switch (type) {
        case SD_BUS_MESSAGE_METHOD_CALL:
            return "SD_BUS_MESSAGE_METHOD_CALL";
        case SD_BUS_MESSAGE_METHOD_RETURN:
            return "SD_BUS_MESSAGE_METHOD_RETURN";
        case SD_BUS_MESSAGE_METHOD_ERROR:
            return "SD_BUS_MESSAGE_METHOD_ERROR";
        case SD_BUS_MESSAGE_SIGNAL:
            return "SD_BUS_MESSAGE_SIGNAL";
    }

    return "unknown type";
}

static void
assign_if_not_empty(json::object& o,
                    cstring_view const name, cstring_view const value)
{
    if (!value.empty()) {
        o[name] = value.c_str();
    }
}

static void
assign_if_not_empty(json::object& o,
                    cstring_view const name,
                    std::optional<duration> const& value)
{
    if (value) {
        o[name] = str::to_string(value.value());
    }
}

template<typename T>
static void
assign_if_not_empty(json::object& o,
                    cstring_view const name, std::optional<T> const& value)
{
    if (value) {
        o[name] = value.value();
    }
}

template<typename T>
T
read_basic_field(message& m, char const type)
{
    T v {};
    m.read_basic(type, &v);

    return v;
}

static bool
read_bool_field(message& m)
{
    int v {};
    m.read_basic(SD_BUS_TYPE_BOOLEAN, &v);

    return v != 0;
}

static json::value
read_field(message& m, message::peek_type_result const&);

static json::array
read_dict_field(message& m, message::peek_type_result const& t)
{
    assert(t.type == SD_BUS_TYPE_DICT_ENTRY);

    m.enter_container(t.type, t.contents);

    json::array result;

    auto o_type = m.peek_type();
    assert(o_type);

    result.push_back(read_field(m, *o_type));

    o_type = m.peek_type();
    assert(o_type);

    result.push_back(read_field(m, *o_type));

    m.exit_container();

    return result;
}

static json::array
read_array_field(message& m, message::peek_type_result const& t)
{
    assert(t.type == SD_BUS_TYPE_ARRAY);

    m.enter_container(t.type, t.contents);

    json::array result;

    auto o_type = m.peek_type();
    assert(o_type);

    if (o_type->type == SD_BUS_TYPE_DICT_ENTRY) {
        while (!m.at_end()) {
            result.push_back(read_dict_field(m, *o_type));
        }
    }
    else {
        while (!m.at_end()) {
            result.push_back(read_field(m, *o_type));
        }
    }

    m.exit_container();

    return result;
}

static json::array
read_struct_field(message& m, message::peek_type_result const& t)
{
    assert(t.type == SD_BUS_TYPE_STRUCT);

    m.enter_container(t.type, t.contents);

    json::array result;

    while (auto o_type = m.peek_type()) {
        result.push_back(read_field(m, *o_type));
    }

    m.exit_container();

    return result;
}

static json::value
read_variant_field(message& m, message::peek_type_result const& t)
{
    assert(t.type == SD_BUS_TYPE_VARIANT);

    m.enter_container(t.type, t.contents);

    json::value result;

    auto o_type = m.peek_type();
    assert(o_type); //TODO

    result = read_field(m, *o_type);

    m.exit_container();

    return result;
}

static json::value
read_field(message& m, message::peek_type_result const& t)
{
    switch (t.type) {
        case SD_BUS_TYPE_BYTE:
            return read_basic_field<uint8_t>(m, t.type);
        case SD_BUS_TYPE_BOOLEAN:
            return read_bool_field(m);
        case SD_BUS_TYPE_INT16:
            return read_basic_field<int16_t>(m, t.type);
        case SD_BUS_TYPE_UINT16:
            return read_basic_field<uint16_t>(m, t.type);
        case SD_BUS_TYPE_INT32:
            return read_basic_field<int32_t>(m, t.type);
        case SD_BUS_TYPE_UINT32:
            return read_basic_field<uint32_t>(m, t.type);
        case SD_BUS_TYPE_INT64:
            return read_basic_field<int64_t>(m, t.type);
        case SD_BUS_TYPE_UINT64:
            return read_basic_field<uint64_t>(m, t.type);
        case SD_BUS_TYPE_DOUBLE:
            return read_basic_field<double>(m, t.type);
        case SD_BUS_TYPE_STRING:
        case SD_BUS_TYPE_OBJECT_PATH:
        case SD_BUS_TYPE_SIGNATURE:
            return read_basic_field<char const*>(m, t.type);
        case SD_BUS_TYPE_UNIX_FD:
            return read_basic_field<int>(m, t.type);
        case SD_BUS_TYPE_ARRAY:
            return read_array_field(m, t);
        case SD_BUS_TYPE_STRUCT:
            return read_struct_field(m, t);
        case SD_BUS_TYPE_VARIANT:
            return read_variant_field(m, t);
        default:
            assert(false); //TODO
    }

    return {};
}

static void
append_fields(json::object& o, message& m)
{
    auto const sig = m.signature(false);
    if (!sig.empty()) {
        o["signature"] = sig.c_str();

        json::array fields;

        while (auto o_type = m.peek_type()) {
            fields.push_back(read_field(m, *o_type));
        }

        o["fields"] = fields;
    }
}

void
tag_invoke(json::value_from_tag, json::value& v, message& m)
{
    m.rewind(true);
    auto& o = v.emplace_object();

    o["type"] = type_to_symbol(m.type());

    assign_if_not_empty(o, "path", m.path());
    assign_if_not_empty(o, "interface", m.interface());
    assign_if_not_empty(o, "member", m.member());
    assign_if_not_empty(o, "destination", m.destination());
    assign_if_not_empty(o, "sender", m.sender());

    append_fields(o, m);

    assign_if_not_empty(o, "cookie", m.cookie());
    assign_if_not_empty(o, "reply_cookie", m.reply_cookie());
    assign_if_not_empty(o, "monotonic_usec", m.monotonic_usec());
    assign_if_not_empty(o, "realtime_usec", m.realtime_usec());
    assign_if_not_empty(o, "seqnum", m.seqnum());

    if (m.type() == SD_BUS_MESSAGE_METHOD_CALL) {
        o["expect_reply"] = m.expect_reply();
    }

    o["auto_start"] = m.auto_start();
    o["allow_interactive_authorization"] = m.allow_interactive_authorization();
}

void
tag_invoke(json::value_from_tag, json::value& v, slot& s)
{
    auto& o = v.emplace_object();

    if (auto const o_d = s.description()) {
        o["description"] = s.description()->c_str();
    }

    o["userdata"] = str::to_string(s.userdata());
    o["is_floating"] = s.is_floating();
}

} // namespace stream9::sd_bus
